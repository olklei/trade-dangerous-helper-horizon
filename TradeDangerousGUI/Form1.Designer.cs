﻿namespace TDHelper
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.runButton = new System.Windows.Forms.Button();
            this.destSysLabel = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.destSystemComboBox = new System.Windows.Forms.ComboBox();
            this.stopWatchLabel = new System.Windows.Forms.Label();
            this.backgroundWorker2 = new System.ComponentModel.BackgroundWorker();
            this.creditsLabel = new System.Windows.Forms.Label();
            this.capacityLabel = new System.Windows.Forms.Label();
            this.unladenLYLabel = new System.Windows.Forms.Label();
            this.ladenLYLabel = new System.Windows.Forms.Label();
            this.towardsCheckBox = new System.Windows.Forms.CheckBox();
            this.loopCheckBox = new System.Windows.Forms.CheckBox();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.cutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pasteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.savePage1MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSaved1MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePage2MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSaved2MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.savePage3MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearSaved3MenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.pushNotesMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pushEDSCToCSV = new System.Windows.Forms.ToolStripMenuItem();
            this.notesClearMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.selectMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveSettingsButton = new System.Windows.Forms.Button();
            this.stationDropDown = new System.Windows.Forms.ComboBox();
            this.oneStopCheckBox = new System.Windows.Forms.CheckBox();
            this.directCheckBox = new System.Windows.Forms.CheckBox();
            this.belowPriceBox = new System.Windows.Forms.NumericUpDown();
            this.abovePriceBox = new System.Windows.Forms.NumericUpDown();
            this.endJumpsBox = new System.Windows.Forms.NumericUpDown();
            this.startJumpsBox = new System.Windows.Forms.NumericUpDown();
            this.limitBox = new System.Windows.Forms.NumericUpDown();
            this.pruneHopsBox = new System.Windows.Forms.NumericUpDown();
            this.pruneScoreBox = new System.Windows.Forms.NumericUpDown();
            this.lsPenaltyBox = new System.Windows.Forms.NumericUpDown();
            this.maxLSDistanceBox = new System.Windows.Forms.NumericUpDown();
            this.gptBox = new System.Windows.Forms.NumericUpDown();
            this.uniqueCheckBox = new System.Windows.Forms.CheckBox();
            this.insuranceBox = new System.Windows.Forms.NumericUpDown();
            this.capacityBox = new System.Windows.Forms.NumericUpDown();
            this.jumpsBox = new System.Windows.Forms.NumericUpDown();
            this.hopsBox = new System.Windows.Forms.NumericUpDown();
            this.creditsBox = new System.Windows.Forms.NumericUpDown();
            this.unladenLYBox = new System.Windows.Forms.NumericUpDown();
            this.ladenLYBox = new System.Windows.Forms.NumericUpDown();
            this.belowPriceLabel = new System.Windows.Forms.Label();
            this.abovePriceLabel = new System.Windows.Forms.Label();
            this.methodDropDown = new System.Windows.Forms.ComboBox();
            this.commodityComboBox = new System.Windows.Forms.ComboBox();
            this.commodityLabel = new System.Windows.Forms.Label();
            this.viaBox = new System.Windows.Forms.TextBox();
            this.viaLabel = new System.Windows.Forms.Label();
            this.startJumpsLabel = new System.Windows.Forms.Label();
            this.endJumpsLabel = new System.Windows.Forms.Label();
            this.avoidBox = new System.Windows.Forms.TextBox();
            this.avoidLabel = new System.Windows.Forms.Label();
            this.bmktCheckBox = new System.Windows.Forms.CheckBox();
            this.localNavCheckBox = new System.Windows.Forms.CheckBox();
            this.hopsLabel = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.updateButton = new System.Windows.Forms.Button();
            this.getSystemButton = new System.Windows.Forms.Button();
            this.srcSysLabel = new System.Windows.Forms.Label();
            this.padSizeLabel = new System.Windows.Forms.Label();
            this.padSizeBox = new System.Windows.Forms.TextBox();
            this.ageLabel = new System.Windows.Forms.Label();
            this.verboseLabel = new System.Windows.Forms.Label();
            this.ageBox = new System.Windows.Forms.NumericUpDown();
            this.editCommodButton = new System.Windows.Forms.Button();
            this.importButton = new System.Windows.Forms.Button();
            this.gptLabel = new System.Windows.Forms.Label();
            this.uploadButton = new System.Windows.Forms.Button();
            this.lsFromStarLabel = new System.Windows.Forms.Label();
            this.maxPadSizeLabel = new System.Windows.Forms.Label();
            this.lsFromStarBox = new System.Windows.Forms.NumericUpDown();
            this.blackMarketCheckBox = new System.Windows.Forms.CheckBox();
            this.marketCheckBox = new System.Windows.Forms.CheckBox();
            this.shipyardCheckBox = new System.Windows.Forms.CheckBox();
            this.stn_padSizeBox = new System.Windows.Forms.TextBox();
            this.shipsSoldLabel = new System.Windows.Forms.Label();
            this.srcSystemComboBox = new System.Windows.Forms.ComboBox();
            this.csvExportCheckBox = new System.Windows.Forms.CheckBox();
            this.csvExportComboBox = new System.Windows.Forms.ComboBox();
            this.marginLabel = new System.Windows.Forms.Label();
            this.marginBox = new System.Windows.Forms.NumericUpDown();
            this.limitLabel = new System.Windows.Forms.Label();
            this.maxGPTLabel = new System.Windows.Forms.Label();
            this.maxGPTBox = new System.Windows.Forms.NumericUpDown();
            this.rearmCheckBox = new System.Windows.Forms.CheckBox();
            this.repairCheckBox = new System.Windows.Forms.CheckBox();
            this.refuelCheckBox = new System.Windows.Forms.CheckBox();
            this.outfitCheckBox = new System.Windows.Forms.CheckBox();
            this.outfitFilterCheckBox = new System.Windows.Forms.CheckBox();
            this.rearmFilterCheckBox = new System.Windows.Forms.CheckBox();
            this.repairFilterCheckBox = new System.Windows.Forms.CheckBox();
            this.refuelFilterCheckBox = new System.Windows.Forms.CheckBox();
            this.shipyardFilterCheckBox = new System.Windows.Forms.CheckBox();
            this.bmktFilterCheckBox = new System.Windows.Forms.CheckBox();
            this.itemsFilterCheckBox = new System.Windows.Forms.CheckBox();
            this.forceUpdateBox = new System.Windows.Forms.ComboBox();
            this.skipImportCheckBox = new System.Windows.Forms.CheckBox();
            this.resetFilterButton = new System.Windows.Forms.Button();
            this.resetStationButton = new System.Windows.Forms.Button();
            this.loadSettingsButton = new System.Windows.Forms.Button();
            this.confirmBox = new System.Windows.Forms.TextBox();
            this.swapButton = new System.Windows.Forms.Button();
            this.miniModeButton = new System.Windows.Forms.Button();
            this.altConfigBox = new System.Windows.Forms.ComboBox();
            this.correctCheckBox = new System.Windows.Forms.CheckBox();
            this.updateNotifyIcon = new System.Windows.Forms.PictureBox();
            this.stockBox = new System.Windows.Forms.NumericUpDown();
            this.stockLabel = new System.Windows.Forms.Label();
            this.testSystemsCheckBox = new System.Windows.Forms.CheckBox();
            this.stationsFilterCheckBox = new System.Windows.Forms.CheckBox();
            this.minAgeUpDown = new System.Windows.Forms.NumericUpDown();
            this.minAgeLabel = new System.Windows.Forms.Label();
            this.oldRoutesCheckBox = new System.Windows.Forms.CheckBox();
            this.edscLYLabel5 = new System.Windows.Forms.Label();
            this.edscLYLabel4 = new System.Windows.Forms.Label();
            this.edscLYLabel3 = new System.Windows.Forms.Label();
            this.edscLYLabel2 = new System.Windows.Forms.Label();
            this.refSysTextBox5 = new System.Windows.Forms.TextBox();
            this.refSysLabel5 = new System.Windows.Forms.Label();
            this.refSysTextBox4 = new System.Windows.Forms.TextBox();
            this.refSysLabel4 = new System.Windows.Forms.Label();
            this.refSysTextBox3 = new System.Windows.Forms.TextBox();
            this.refSysLabel3 = new System.Windows.Forms.Label();
            this.refSysTextBox2 = new System.Windows.Forms.TextBox();
            this.refSysLabel2 = new System.Windows.Forms.Label();
            this.edscLYLabel1 = new System.Windows.Forms.Label();
            this.refSysTextBox1 = new System.Windows.Forms.TextBox();
            this.refSysLabel1 = new System.Windows.Forms.Label();
            this.cmdrNameLabel = new System.Windows.Forms.Label();
            this.cmdrNameTextBox = new System.Windows.Forms.TextBox();
            this.loopIntLabel = new System.Windows.Forms.Label();
            this.loopIntBox = new System.Windows.Forms.NumericUpDown();
            this.edscLYBox1 = new System.Windows.Forms.NumericUpDown();
            this.edscLYBox2 = new System.Windows.Forms.NumericUpDown();
            this.edscLYBox3 = new System.Windows.Forms.NumericUpDown();
            this.edscLYBox4 = new System.Windows.Forms.NumericUpDown();
            this.edscLYBox5 = new System.Windows.Forms.NumericUpDown();
            this.shortenCheckBox = new System.Windows.Forms.CheckBox();
            this.shipsSoldBox = new System.Windows.Forms.ComboBox();
            this.crFilterLabel = new System.Windows.Forms.Label();
            this.crFilterUpDown = new System.Windows.Forms.NumericUpDown();
            this.demandBox = new System.Windows.Forms.NumericUpDown();
            this.demandLabel = new System.Windows.Forms.Label();
            this.showJumpsCheckBox = new System.Windows.Forms.CheckBox();
            this.miscSettingsButton = new System.Windows.Forms.Button();
            this.verbosityComboBox = new System.Windows.Forms.ComboBox();
            this.backgroundWorker3 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker4 = new System.ComponentModel.BackgroundWorker();
            this.panel1 = new System.Windows.Forms.Panel();
            this.updateNotifyLabel = new System.Windows.Forms.Label();
            this.localFilterParentPanel = new System.Windows.Forms.Panel();
            this.localFilterGroupBox = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.outputPage = new System.Windows.Forms.TabPage();
            this.td_outputBox = new System.Windows.Forms.RichTextBox();
            this.savedPage1 = new System.Windows.Forms.TabPage();
            this.savedTextBox1 = new System.Windows.Forms.RichTextBox();
            this.savedPage2 = new System.Windows.Forms.TabPage();
            this.savedTextBox2 = new System.Windows.Forms.RichTextBox();
            this.savedPage3 = new System.Windows.Forms.TabPage();
            this.savedTextBox3 = new System.Windows.Forms.RichTextBox();
            this.notesPage = new System.Windows.Forms.TabPage();
            this.notesTextBox = new System.Windows.Forms.RichTextBox();
            this.logPage = new System.Windows.Forms.TabPage();
            this.pilotsLogDataGrid = new System.Windows.Forms.DataGridView();
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.insertAtGridRow = new System.Windows.Forms.ToolStripMenuItem();
            this.removeAtGridRow = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.forceRefreshGridView = new System.Windows.Forms.ToolStripMenuItem();
            this.forceResortMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.copySystemToSrc = new System.Windows.Forms.ToolStripMenuItem();
            this.copySystemToDest = new System.Windows.Forms.ToolStripMenuItem();
            this.jumpsLabel = new System.Windows.Forms.Label();
            this.maxLSLabel = new System.Windows.Forms.Label();
            this.pruneHopsLabel = new System.Windows.Forms.Label();
            this.pruneScoreLabel = new System.Windows.Forms.Label();
            this.lsPenaltyLabel = new System.Windows.Forms.Label();
            this.insuranceLabel = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panelLocalOverrideChild = new System.Windows.Forms.Panel();
            this.confirmLabel = new System.Windows.Forms.Label();
            this.runOptionsPanel = new System.Windows.Forms.Panel();
            this.backgroundWorker5 = new System.ComponentModel.BackgroundWorker();
            this.backgroundWorker6 = new System.ComponentModel.BackgroundWorker();
            this.edscPanel = new System.Windows.Forms.Panel();
            this.trackerLinkLabel = new System.Windows.Forms.LinkLabel();
            this.faqLinkLabel = new System.Windows.Forms.LinkLabel();
            this.copySystemToEDSC = new System.Windows.Forms.ToolStripMenuItem();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.belowPriceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.abovePriceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.endJumpsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.startJumpsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.limitBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pruneHopsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pruneScoreBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsPenaltyBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxLSDistanceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gptBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.insuranceBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacityBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.jumpsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.hopsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditsBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.unladenLYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ladenLYBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ageBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsFromStarBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.marginBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxGPTBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateNotifyIcon)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.minAgeUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.loopIntBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.crFilterUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.demandBox)).BeginInit();
            this.panel1.SuspendLayout();
            this.localFilterParentPanel.SuspendLayout();
            this.localFilterGroupBox.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.outputPage.SuspendLayout();
            this.savedPage1.SuspendLayout();
            this.savedPage2.SuspendLayout();
            this.savedPage3.SuspendLayout();
            this.notesPage.SuspendLayout();
            this.logPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pilotsLogDataGrid)).BeginInit();
            this.contextMenuStrip2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panelLocalOverrideChild.SuspendLayout();
            this.runOptionsPanel.SuspendLayout();
            this.edscPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // runButton
            // 
            this.runButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.runButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.runButton.Location = new System.Drawing.Point(735, 11);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(78, 22);
            this.runButton.TabIndex = 1;
            this.runButton.TabStop = false;
            this.runButton.Text = "&Run";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.runButton_Click);
            // 
            // destSysLabel
            // 
            this.destSysLabel.AutoSize = true;
            this.destSysLabel.Location = new System.Drawing.Point(12, 11);
            this.destSysLabel.Name = "destSysLabel";
            this.destSysLabel.Size = new System.Drawing.Size(63, 13);
            this.destSysLabel.TabIndex = 1;
            this.destSysLabel.Text = "Destination:";
            this.toolTip1.SetToolTip(this.destSysLabel, "Destination point in the form of system or system/station");
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // destSystemComboBox
            // 
            this.destSystemComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.destSystemComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.destSystemComboBox.Location = new System.Drawing.Point(75, 8);
            this.destSystemComboBox.Name = "destSystemComboBox";
            this.destSystemComboBox.Size = new System.Drawing.Size(230, 21);
            this.destSystemComboBox.TabIndex = 0;
            this.toolTip1.SetToolTip(this.destSystemComboBox, "Destination point in the form of system or system/station\r\nCtrl+Enter adds a Syst" +
        "em/Station to the favorites\r\nShift+Enter removes a System/Station from the favor" +
        "ites");
            this.destSystemComboBox.DropDown += new System.EventHandler(this.comboBox_DropDown);
            this.destSystemComboBox.DropDownClosed += new System.EventHandler(this.comboBox_DropDownClosed);
            this.destSystemComboBox.TextChanged += new System.EventHandler(this.destSystemComboBox_TextChanged);
            this.destSystemComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.destSystemComboBox_KeyDown);
            // 
            // stopWatchLabel
            // 
            this.stopWatchLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.stopWatchLabel.AutoSize = true;
            this.stopWatchLabel.Font = new System.Drawing.Font("Consolas", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.stopWatchLabel.Location = new System.Drawing.Point(13, 717);
            this.stopWatchLabel.Name = "stopWatchLabel";
            this.stopWatchLabel.Size = new System.Drawing.Size(0, 15);
            this.stopWatchLabel.TabIndex = 9;
            // 
            // backgroundWorker2
            // 
            this.backgroundWorker2.WorkerReportsProgress = true;
            this.backgroundWorker2.WorkerSupportsCancellation = true;
            this.backgroundWorker2.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker2_DoWork);
            this.backgroundWorker2.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker2_RunWorkerCompleted);
            // 
            // creditsLabel
            // 
            this.creditsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.creditsLabel.AutoSize = true;
            this.creditsLabel.Location = new System.Drawing.Point(198, 33);
            this.creditsLabel.Name = "creditsLabel";
            this.creditsLabel.Size = new System.Drawing.Size(42, 13);
            this.creditsLabel.TabIndex = 0;
            this.creditsLabel.Text = "Credits:";
            this.toolTip1.SetToolTip(this.creditsLabel, "Current credits");
            // 
            // capacityLabel
            // 
            this.capacityLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.capacityLabel.AutoSize = true;
            this.capacityLabel.Location = new System.Drawing.Point(357, 56);
            this.capacityLabel.Name = "capacityLabel";
            this.capacityLabel.Size = new System.Drawing.Size(51, 13);
            this.capacityLabel.TabIndex = 1;
            this.capacityLabel.Text = "Capacity:";
            this.toolTip1.SetToolTip(this.capacityLabel, "Total cargo space in your ship");
            // 
            // unladenLYLabel
            // 
            this.unladenLYLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.unladenLYLabel.AutoSize = true;
            this.unladenLYLabel.Location = new System.Drawing.Point(342, 33);
            this.unladenLYLabel.Name = "unladenLYLabel";
            this.unladenLYLabel.Size = new System.Drawing.Size(66, 13);
            this.unladenLYLabel.TabIndex = 3;
            this.unladenLYLabel.Text = "Unladen LY:";
            this.toolTip1.SetToolTip(this.unladenLYLabel, "Distance that can be travelled while unladen (including fuel)");
            // 
            // ladenLYLabel
            // 
            this.ladenLYLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ladenLYLabel.AutoSize = true;
            this.ladenLYLabel.Location = new System.Drawing.Point(352, 10);
            this.ladenLYLabel.Name = "ladenLYLabel";
            this.ladenLYLabel.Size = new System.Drawing.Size(56, 13);
            this.ladenLYLabel.TabIndex = 2;
            this.ladenLYLabel.Text = "Laden LY:";
            this.toolTip1.SetToolTip(this.ladenLYLabel, "Distance that can be travelled while fully laden (including fuel)");
            // 
            // towardsCheckBox
            // 
            this.towardsCheckBox.AutoSize = true;
            this.towardsCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.towardsCheckBox.Location = new System.Drawing.Point(238, 32);
            this.towardsCheckBox.Name = "towardsCheckBox";
            this.towardsCheckBox.Size = new System.Drawing.Size(67, 17);
            this.towardsCheckBox.TabIndex = 7;
            this.towardsCheckBox.TabStop = false;
            this.towardsCheckBox.Text = "Towards";
            this.toolTip1.SetToolTip(this.towardsCheckBox, "Favors distance covered over profit generated during routing (requires a Destinat" +
        "ion)");
            this.towardsCheckBox.UseVisualStyleBackColor = true;
            this.towardsCheckBox.Click += new System.EventHandler(this.towardsCheckBox_Click);
            // 
            // loopCheckBox
            // 
            this.loopCheckBox.AutoSize = true;
            this.loopCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.loopCheckBox.Location = new System.Drawing.Point(181, 32);
            this.loopCheckBox.Name = "loopCheckBox";
            this.loopCheckBox.Size = new System.Drawing.Size(50, 17);
            this.loopCheckBox.TabIndex = 6;
            this.loopCheckBox.TabStop = false;
            this.loopCheckBox.Text = "Loop";
            this.toolTip1.SetToolTip(this.loopCheckBox, "Attempts to get a round-trip route");
            this.loopCheckBox.UseVisualStyleBackColor = true;
            this.loopCheckBox.Click += new System.EventHandler(this.loopCheckBox_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cutMenuItem,
            this.copyMenuItem,
            this.pasteMenuItem,
            this.deleteMenuItem,
            this.toolStripSeparator3,
            this.savePage1MenuItem,
            this.clearSaved1MenuItem,
            this.savePage2MenuItem,
            this.clearSaved2MenuItem,
            this.savePage3MenuItem,
            this.clearSaved3MenuItem,
            this.toolStripSeparator2,
            this.pushNotesMenuItem,
            this.pushEDSCToCSV,
            this.notesClearMenuItem,
            this.toolStripSeparator1,
            this.selectMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.System;
            this.contextMenuStrip1.ShowImageMargin = false;
            this.contextMenuStrip1.Size = new System.Drawing.Size(122, 330);
            this.contextMenuStrip1.Opening += new System.ComponentModel.CancelEventHandler(this.contextMenuStrip1_Opening);
            // 
            // cutMenuItem
            // 
            this.cutMenuItem.Enabled = false;
            this.cutMenuItem.Name = "cutMenuItem";
            this.cutMenuItem.Size = new System.Drawing.Size(121, 22);
            this.cutMenuItem.Text = "Cut";
            this.cutMenuItem.Click += new System.EventHandler(this.cutMenuItem_Click);
            // 
            // copyMenuItem
            // 
            this.copyMenuItem.Name = "copyMenuItem";
            this.copyMenuItem.Size = new System.Drawing.Size(121, 22);
            this.copyMenuItem.Text = "Copy";
            this.copyMenuItem.Click += new System.EventHandler(this.copyMenuItem_Click);
            // 
            // pasteMenuItem
            // 
            this.pasteMenuItem.Enabled = false;
            this.pasteMenuItem.Name = "pasteMenuItem";
            this.pasteMenuItem.Size = new System.Drawing.Size(121, 22);
            this.pasteMenuItem.Text = "Paste";
            this.pasteMenuItem.Click += new System.EventHandler(this.pasteMenuItem_Click);
            // 
            // deleteMenuItem
            // 
            this.deleteMenuItem.Enabled = false;
            this.deleteMenuItem.Name = "deleteMenuItem";
            this.deleteMenuItem.Size = new System.Drawing.Size(121, 22);
            this.deleteMenuItem.Text = "Delete";
            this.deleteMenuItem.Click += new System.EventHandler(this.deleteMenuItem_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(118, 6);
            // 
            // savePage1MenuItem
            // 
            this.savePage1MenuItem.Enabled = false;
            this.savePage1MenuItem.Name = "savePage1MenuItem";
            this.savePage1MenuItem.Size = new System.Drawing.Size(121, 22);
            this.savePage1MenuItem.Text = "Save to #1";
            this.savePage1MenuItem.Click += new System.EventHandler(this.savePage1MenuItem_Click);
            // 
            // clearSaved1MenuItem
            // 
            this.clearSaved1MenuItem.Name = "clearSaved1MenuItem";
            this.clearSaved1MenuItem.Size = new System.Drawing.Size(121, 22);
            this.clearSaved1MenuItem.Text = "Clear #1";
            this.clearSaved1MenuItem.Visible = false;
            this.clearSaved1MenuItem.Click += new System.EventHandler(this.clearSaved1MenuItem_Click);
            // 
            // savePage2MenuItem
            // 
            this.savePage2MenuItem.Enabled = false;
            this.savePage2MenuItem.Name = "savePage2MenuItem";
            this.savePage2MenuItem.Size = new System.Drawing.Size(121, 22);
            this.savePage2MenuItem.Text = "Save to #2";
            this.savePage2MenuItem.Click += new System.EventHandler(this.savePage2MenuItem_Click);
            // 
            // clearSaved2MenuItem
            // 
            this.clearSaved2MenuItem.Name = "clearSaved2MenuItem";
            this.clearSaved2MenuItem.Size = new System.Drawing.Size(121, 22);
            this.clearSaved2MenuItem.Text = "Clear #2";
            this.clearSaved2MenuItem.Visible = false;
            this.clearSaved2MenuItem.Click += new System.EventHandler(this.clearSaved2MenuItem_Click);
            // 
            // savePage3MenuItem
            // 
            this.savePage3MenuItem.Enabled = false;
            this.savePage3MenuItem.Name = "savePage3MenuItem";
            this.savePage3MenuItem.Size = new System.Drawing.Size(121, 22);
            this.savePage3MenuItem.Text = "Save to #3";
            this.savePage3MenuItem.Click += new System.EventHandler(this.savePage3MenuItem_Click);
            // 
            // clearSaved3MenuItem
            // 
            this.clearSaved3MenuItem.Name = "clearSaved3MenuItem";
            this.clearSaved3MenuItem.Size = new System.Drawing.Size(121, 22);
            this.clearSaved3MenuItem.Text = "Clear #3";
            this.clearSaved3MenuItem.Visible = false;
            this.clearSaved3MenuItem.Click += new System.EventHandler(this.clearSaved3MenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(118, 6);
            // 
            // pushNotesMenuItem
            // 
            this.pushNotesMenuItem.Name = "pushNotesMenuItem";
            this.pushNotesMenuItem.Size = new System.Drawing.Size(121, 22);
            this.pushNotesMenuItem.Text = "Add To Notes";
            this.pushNotesMenuItem.Click += new System.EventHandler(this.pushNotesMenuItem_Click);
            // 
            // pushEDSCToCSV
            // 
            this.pushEDSCToCSV.Name = "pushEDSCToCSV";
            this.pushEDSCToCSV.Size = new System.Drawing.Size(121, 22);
            this.pushEDSCToCSV.Text = "Add To CSV";
            this.pushEDSCToCSV.Visible = false;
            this.pushEDSCToCSV.Click += new System.EventHandler(this.pushEDSCToCSV_Click);
            // 
            // notesClearMenuItem
            // 
            this.notesClearMenuItem.Name = "notesClearMenuItem";
            this.notesClearMenuItem.Size = new System.Drawing.Size(121, 22);
            this.notesClearMenuItem.Text = "Clear Notes";
            this.notesClearMenuItem.Visible = false;
            this.notesClearMenuItem.Click += new System.EventHandler(this.notesClearMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(118, 6);
            // 
            // selectMenuItem
            // 
            this.selectMenuItem.Name = "selectMenuItem";
            this.selectMenuItem.Size = new System.Drawing.Size(121, 22);
            this.selectMenuItem.Text = "Select All";
            this.selectMenuItem.Click += new System.EventHandler(this.selectMenuItem_Click);
            // 
            // saveSettingsButton
            // 
            this.saveSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.saveSettingsButton.Location = new System.Drawing.Point(731, 713);
            this.saveSettingsButton.Name = "saveSettingsButton";
            this.saveSettingsButton.Size = new System.Drawing.Size(82, 23);
            this.saveSettingsButton.TabIndex = 28;
            this.saveSettingsButton.TabStop = false;
            this.saveSettingsButton.Text = "Save Settings";
            this.toolTip1.SetToolTip(this.saveSettingsButton, "Ctrl+Click to choose a different settings file to save");
            this.saveSettingsButton.UseVisualStyleBackColor = true;
            this.saveSettingsButton.Click += new System.EventHandler(this.saveSettingsButton_Click);
            // 
            // stationDropDown
            // 
            this.stationDropDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.stationDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stationDropDown.Items.AddRange(new object[] {
            "Update",
            "Add",
            "Remove"});
            this.stationDropDown.Location = new System.Drawing.Point(10, 54);
            this.stationDropDown.Name = "stationDropDown";
            this.stationDropDown.Size = new System.Drawing.Size(82, 21);
            this.stationDropDown.TabIndex = 17;
            this.stationDropDown.TabStop = false;
            this.toolTip1.SetToolTip(this.stationDropDown, "Select a mode for station editing");
            this.stationDropDown.Visible = false;
            this.stationDropDown.SelectedIndexChanged += new System.EventHandler(this.stationDropDown_SelectedIndexChanged);
            // 
            // oneStopCheckBox
            // 
            this.oneStopCheckBox.AutoSize = true;
            this.oneStopCheckBox.Enabled = false;
            this.oneStopCheckBox.Location = new System.Drawing.Point(10, 121);
            this.oneStopCheckBox.Name = "oneStopCheckBox";
            this.oneStopCheckBox.Size = new System.Drawing.Size(55, 17);
            this.oneStopCheckBox.TabIndex = 8;
            this.oneStopCheckBox.TabStop = false;
            this.oneStopCheckBox.Text = "1-stop";
            this.toolTip1.SetToolTip(this.oneStopCheckBox, "Filters stations that don\'t contain all commodities searched for (volatile)");
            this.oneStopCheckBox.UseVisualStyleBackColor = true;
            // 
            // directCheckBox
            // 
            this.directCheckBox.AutoSize = true;
            this.directCheckBox.Location = new System.Drawing.Point(10, 77);
            this.directCheckBox.Name = "directCheckBox";
            this.directCheckBox.Size = new System.Drawing.Size(54, 17);
            this.directCheckBox.TabIndex = 3;
            this.directCheckBox.TabStop = false;
            this.directCheckBox.Text = "Direct";
            this.toolTip1.SetToolTip(this.directCheckBox, "Attempts to calculate a 1 hop route, ignoring distance (volatile)");
            this.directCheckBox.UseVisualStyleBackColor = true;
            this.directCheckBox.Click += new System.EventHandler(this.directCheckBox_Click);
            // 
            // belowPriceBox
            // 
            this.belowPriceBox.Location = new System.Drawing.Point(251, 120);
            this.belowPriceBox.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.belowPriceBox.Name = "belowPriceBox";
            this.belowPriceBox.Size = new System.Drawing.Size(55, 20);
            this.belowPriceBox.TabIndex = 5;
            this.belowPriceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.belowPriceBox.ThousandsSeparator = true;
            this.toolTip1.SetToolTip(this.belowPriceBox, "Generates this many routes for a Run");
            this.belowPriceBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.belowPriceBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // abovePriceBox
            // 
            this.abovePriceBox.Location = new System.Drawing.Point(134, 120);
            this.abovePriceBox.Maximum = new decimal(new int[] {
            50000,
            0,
            0,
            0});
            this.abovePriceBox.Name = "abovePriceBox";
            this.abovePriceBox.Size = new System.Drawing.Size(55, 20);
            this.abovePriceBox.TabIndex = 4;
            this.abovePriceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.abovePriceBox.ThousandsSeparator = true;
            this.toolTip1.SetToolTip(this.abovePriceBox, "Commodities above this price are filtered out");
            this.abovePriceBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.abovePriceBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // endJumpsBox
            // 
            this.endJumpsBox.Location = new System.Drawing.Point(134, 55);
            this.endJumpsBox.Name = "endJumpsBox";
            this.endJumpsBox.Size = new System.Drawing.Size(46, 20);
            this.endJumpsBox.TabIndex = 1;
            this.endJumpsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.endJumpsBox, "Try to finish this many jumps away from the destination, this requires a destinat" +
        "ion (volatile)");
            this.endJumpsBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.endJumpsBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // startJumpsBox
            // 
            this.startJumpsBox.Location = new System.Drawing.Point(259, 54);
            this.startJumpsBox.Name = "startJumpsBox";
            this.startJumpsBox.Size = new System.Drawing.Size(46, 20);
            this.startJumpsBox.TabIndex = 2;
            this.startJumpsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.startJumpsBox, "Try to route starting from this many jumps away from the source system (volatile)" +
        "");
            this.startJumpsBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.startJumpsBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // limitBox
            // 
            this.limitBox.Location = new System.Drawing.Point(286, 100);
            this.limitBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.limitBox.Name = "limitBox";
            this.limitBox.Size = new System.Drawing.Size(55, 20);
            this.limitBox.TabIndex = 11;
            this.limitBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.limitBox, "Limit each commodity purchased to this amount on a hop");
            this.limitBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.limitBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // pruneHopsBox
            // 
            this.pruneHopsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pruneHopsBox.Location = new System.Drawing.Point(82, 147);
            this.pruneHopsBox.Name = "pruneHopsBox";
            this.pruneHopsBox.Size = new System.Drawing.Size(46, 20);
            this.pruneHopsBox.TabIndex = 16;
            this.pruneHopsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.pruneHopsBox, "Number of hops before pruning starts, to enable set >=2");
            this.pruneHopsBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.pruneHopsBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // pruneScoreBox
            // 
            this.pruneScoreBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pruneScoreBox.Location = new System.Drawing.Point(82, 170);
            this.pruneScoreBox.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.pruneScoreBox.Name = "pruneScoreBox";
            this.pruneScoreBox.Size = new System.Drawing.Size(46, 20);
            this.pruneScoreBox.TabIndex = 17;
            this.pruneScoreBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.pruneScoreBox, "Percentage of route score change, below which pruning occurs");
            this.pruneScoreBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.pruneScoreBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // lsPenaltyBox
            // 
            this.lsPenaltyBox.Location = new System.Drawing.Point(163, 123);
            this.lsPenaltyBox.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.lsPenaltyBox.Name = "lsPenaltyBox";
            this.lsPenaltyBox.Size = new System.Drawing.Size(55, 20);
            this.lsPenaltyBox.TabIndex = 13;
            this.lsPenaltyBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.lsPenaltyBox, "Scoring penalty per LS traveled to station");
            this.lsPenaltyBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.lsPenaltyBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // maxLSDistanceBox
            // 
            this.maxLSDistanceBox.Location = new System.Drawing.Point(286, 123);
            this.maxLSDistanceBox.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.maxLSDistanceBox.Name = "maxLSDistanceBox";
            this.maxLSDistanceBox.Size = new System.Drawing.Size(55, 20);
            this.maxLSDistanceBox.TabIndex = 14;
            this.maxLSDistanceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.maxLSDistanceBox, "Maximum distance station can be from system drop");
            this.maxLSDistanceBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.maxLSDistanceBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // gptBox
            // 
            this.gptBox.Location = new System.Drawing.Point(409, 77);
            this.gptBox.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.gptBox.Name = "gptBox";
            this.gptBox.Size = new System.Drawing.Size(60, 20);
            this.gptBox.TabIndex = 9;
            this.gptBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.gptBox.ThousandsSeparator = true;
            this.toolTip1.SetToolTip(this.gptBox, "Minimum profit in credits per ton on any hop");
            this.gptBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.gptBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // uniqueCheckBox
            // 
            this.uniqueCheckBox.AutoSize = true;
            this.uniqueCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.uniqueCheckBox.Location = new System.Drawing.Point(44, 32);
            this.uniqueCheckBox.Name = "uniqueCheckBox";
            this.uniqueCheckBox.Size = new System.Drawing.Size(60, 17);
            this.uniqueCheckBox.TabIndex = 5;
            this.uniqueCheckBox.TabStop = false;
            this.uniqueCheckBox.Text = "Unique";
            this.toolTip1.SetToolTip(this.uniqueCheckBox, "Require that stations on a route only be visited once");
            this.uniqueCheckBox.UseVisualStyleBackColor = true;
            // 
            // insuranceBox
            // 
            this.insuranceBox.Location = new System.Drawing.Point(241, 54);
            this.insuranceBox.Maximum = new decimal(new int[] {
            -2147483648,
            2,
            0,
            0});
            this.insuranceBox.Name = "insuranceBox";
            this.insuranceBox.Size = new System.Drawing.Size(100, 20);
            this.insuranceBox.TabIndex = 5;
            this.insuranceBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.insuranceBox.ThousandsSeparator = true;
            this.toolTip1.SetToolTip(this.insuranceBox, "Keep at least this much in credits during routing");
            this.insuranceBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.insuranceBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // capacityBox
            // 
            this.capacityBox.Location = new System.Drawing.Point(409, 54);
            this.capacityBox.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.capacityBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.capacityBox.Name = "capacityBox";
            this.capacityBox.Size = new System.Drawing.Size(60, 20);
            this.capacityBox.TabIndex = 6;
            this.capacityBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.capacityBox, "Total cargo space in your ship");
            this.capacityBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.capacityBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.capacityBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // jumpsBox
            // 
            this.jumpsBox.Location = new System.Drawing.Point(295, 8);
            this.jumpsBox.Name = "jumpsBox";
            this.jumpsBox.Size = new System.Drawing.Size(46, 20);
            this.jumpsBox.TabIndex = 1;
            this.jumpsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.jumpsBox, "A jump is any system between two hops");
            this.jumpsBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.jumpsBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // hopsBox
            // 
            this.hopsBox.Location = new System.Drawing.Point(197, 8);
            this.hopsBox.Name = "hopsBox";
            this.hopsBox.Size = new System.Drawing.Size(46, 20);
            this.hopsBox.TabIndex = 0;
            this.hopsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.hopsBox, "A hop is a station to load/unload from");
            this.hopsBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.hopsBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // creditsBox
            // 
            this.creditsBox.Location = new System.Drawing.Point(241, 31);
            this.creditsBox.Maximum = new decimal(new int[] {
            -2147483648,
            2,
            0,
            0});
            this.creditsBox.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.creditsBox.Name = "creditsBox";
            this.creditsBox.Size = new System.Drawing.Size(100, 20);
            this.creditsBox.TabIndex = 3;
            this.creditsBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.creditsBox.ThousandsSeparator = true;
            this.toolTip1.SetToolTip(this.creditsBox, "Current credits");
            this.creditsBox.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.creditsBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.creditsBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // unladenLYBox
            // 
            this.unladenLYBox.DecimalPlaces = 2;
            this.unladenLYBox.Location = new System.Drawing.Point(409, 31);
            this.unladenLYBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            131072});
            this.unladenLYBox.Name = "unladenLYBox";
            this.unladenLYBox.Size = new System.Drawing.Size(60, 20);
            this.unladenLYBox.TabIndex = 4;
            this.unladenLYBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.unladenLYBox, "Distance that can be travelled while unladen (including fuel)");
            this.unladenLYBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.unladenLYBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // ladenLYBox
            // 
            this.ladenLYBox.DecimalPlaces = 2;
            this.ladenLYBox.Location = new System.Drawing.Point(409, 8);
            this.ladenLYBox.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            131072});
            this.ladenLYBox.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ladenLYBox.Name = "ladenLYBox";
            this.ladenLYBox.Size = new System.Drawing.Size(60, 20);
            this.ladenLYBox.TabIndex = 2;
            this.ladenLYBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.ladenLYBox, "Distance that can be travelled while fully laden (including fuel)");
            this.ladenLYBox.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.ladenLYBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.ladenLYBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // belowPriceLabel
            // 
            this.belowPriceLabel.AutoSize = true;
            this.belowPriceLabel.Location = new System.Drawing.Point(207, 122);
            this.belowPriceLabel.Name = "belowPriceLabel";
            this.belowPriceLabel.Size = new System.Drawing.Size(44, 13);
            this.belowPriceLabel.TabIndex = 58;
            this.belowPriceLabel.Tag = "";
            this.belowPriceLabel.Text = "Routes:";
            this.toolTip1.SetToolTip(this.belowPriceLabel, "Generates this many routes for a Run");
            // 
            // abovePriceLabel
            // 
            this.abovePriceLabel.AutoSize = true;
            this.abovePriceLabel.Location = new System.Drawing.Point(93, 122);
            this.abovePriceLabel.Name = "abovePriceLabel";
            this.abovePriceLabel.Size = new System.Drawing.Size(41, 13);
            this.abovePriceLabel.TabIndex = 56;
            this.abovePriceLabel.Text = "Above:";
            this.toolTip1.SetToolTip(this.abovePriceLabel, "Commodities above this price are filtered out");
            // 
            // methodDropDown
            // 
            this.methodDropDown.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.methodDropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.methodDropDown.Items.AddRange(new object[] {
            "Run",
            "Buy",
            "Sell",
            "Rares",
            "Trade",
            "Market",
            "Station",
            "ShipVendor",
            "Navigation",
            "OldData",
            "EDSC"});
            this.methodDropDown.Location = new System.Drawing.Point(350, 66);
            this.methodDropDown.Name = "methodDropDown";
            this.methodDropDown.Size = new System.Drawing.Size(82, 21);
            this.methodDropDown.TabIndex = 1;
            this.methodDropDown.TabStop = false;
            this.toolTip1.SetToolTip(this.methodDropDown, "Select the command to run");
            this.methodDropDown.SelectedIndexChanged += new System.EventHandler(this.methodComboBox_SelectedIndexChanged);
            // 
            // commodityComboBox
            // 
            this.commodityComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.commodityComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.commodityComboBox.Location = new System.Drawing.Point(134, 97);
            this.commodityComboBox.Name = "commodityComboBox";
            this.commodityComboBox.Size = new System.Drawing.Size(172, 21);
            this.commodityComboBox.TabIndex = 3;
            this.toolTip1.SetToolTip(this.commodityComboBox, "Commodities to search for using Buy/Sell, and can be delimited by comma");
            this.commodityComboBox.DropDown += new System.EventHandler(this.comboBox_DropDown);
            this.commodityComboBox.DropDownClosed += new System.EventHandler(this.comboBox_DropDownClosed);
            // 
            // commodityLabel
            // 
            this.commodityLabel.AutoSize = true;
            this.commodityLabel.Enabled = false;
            this.commodityLabel.Location = new System.Drawing.Point(73, 100);
            this.commodityLabel.Name = "commodityLabel";
            this.commodityLabel.Size = new System.Drawing.Size(61, 13);
            this.commodityLabel.TabIndex = 31;
            this.commodityLabel.Text = "Commodity:";
            this.toolTip1.SetToolTip(this.commodityLabel, "Commodities to search for using Buy/Sell, and can be delimited by comma");
            // 
            // viaBox
            // 
            this.viaBox.Location = new System.Drawing.Point(227, 169);
            this.viaBox.Name = "viaBox";
            this.viaBox.Size = new System.Drawing.Size(242, 20);
            this.viaBox.TabIndex = 19;
            this.viaBox.TabStop = false;
            this.toolTip1.SetToolTip(this.viaBox, "Attempt to route through these systems, delimited by comma");
            // 
            // viaLabel
            // 
            this.viaLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.viaLabel.AutoSize = true;
            this.viaLabel.Location = new System.Drawing.Point(196, 174);
            this.viaLabel.Name = "viaLabel";
            this.viaLabel.Size = new System.Drawing.Size(25, 13);
            this.viaLabel.TabIndex = 45;
            this.viaLabel.Text = "Via:";
            this.toolTip1.SetToolTip(this.viaLabel, "Attempt to route through these systems, delimited by comma");
            // 
            // startJumpsLabel
            // 
            this.startJumpsLabel.AutoSize = true;
            this.startJumpsLabel.Location = new System.Drawing.Point(194, 56);
            this.startJumpsLabel.Name = "startJumpsLabel";
            this.startJumpsLabel.Size = new System.Drawing.Size(65, 13);
            this.startJumpsLabel.TabIndex = 43;
            this.startJumpsLabel.Text = "Start Jumps:";
            this.toolTip1.SetToolTip(this.startJumpsLabel, "Try to route starting from this many jumps away from the source system (volatile)" +
        "");
            // 
            // endJumpsLabel
            // 
            this.endJumpsLabel.AutoSize = true;
            this.endJumpsLabel.Enabled = false;
            this.endJumpsLabel.Location = new System.Drawing.Point(72, 57);
            this.endJumpsLabel.Name = "endJumpsLabel";
            this.endJumpsLabel.Size = new System.Drawing.Size(62, 13);
            this.endJumpsLabel.TabIndex = 41;
            this.endJumpsLabel.Text = "End Jumps:";
            this.toolTip1.SetToolTip(this.endJumpsLabel, "Try to finish this many jumps away from the destination, this requires a destinat" +
        "ion (volatile)");
            // 
            // avoidBox
            // 
            this.avoidBox.Location = new System.Drawing.Point(227, 146);
            this.avoidBox.Name = "avoidBox";
            this.avoidBox.Size = new System.Drawing.Size(242, 20);
            this.avoidBox.TabIndex = 18;
            this.avoidBox.TabStop = false;
            this.toolTip1.SetToolTip(this.avoidBox, "Avoids can include system/station and items delimited by comma");
            this.avoidBox.TextChanged += new System.EventHandler(this.avoidBox_TextChanged);
            // 
            // avoidLabel
            // 
            this.avoidLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.avoidLabel.AutoSize = true;
            this.avoidLabel.Location = new System.Drawing.Point(184, 149);
            this.avoidLabel.Name = "avoidLabel";
            this.avoidLabel.Size = new System.Drawing.Size(37, 13);
            this.avoidLabel.TabIndex = 39;
            this.avoidLabel.Text = "Avoid:";
            this.toolTip1.SetToolTip(this.avoidLabel, "Avoids can include system/station and items delimited by comma");
            // 
            // bmktCheckBox
            // 
            this.bmktCheckBox.AutoSize = true;
            this.bmktCheckBox.Location = new System.Drawing.Point(10, 55);
            this.bmktCheckBox.Name = "bmktCheckBox";
            this.bmktCheckBox.Size = new System.Drawing.Size(51, 17);
            this.bmktCheckBox.TabIndex = 2;
            this.bmktCheckBox.TabStop = false;
            this.bmktCheckBox.Text = "BMkt";
            this.toolTip1.SetToolTip(this.bmktCheckBox, "Require stations with a black market");
            this.bmktCheckBox.UseVisualStyleBackColor = true;
            this.bmktCheckBox.Click += new System.EventHandler(this.bmktCheckBox_Click);
            // 
            // localNavCheckBox
            // 
            this.localNavCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.localNavCheckBox.AutoSize = true;
            this.localNavCheckBox.Location = new System.Drawing.Point(367, 114);
            this.localNavCheckBox.Name = "localNavCheckBox";
            this.localNavCheckBox.Size = new System.Drawing.Size(52, 17);
            this.localNavCheckBox.TabIndex = 2;
            this.localNavCheckBox.TabStop = false;
            this.localNavCheckBox.Text = "&Local";
            this.toolTip1.SetToolTip(this.localNavCheckBox, "Enable this for a local station check (volatile)");
            this.localNavCheckBox.UseVisualStyleBackColor = true;
            this.localNavCheckBox.CheckedChanged += new System.EventHandler(this.localNavBox_CheckedChanged);
            // 
            // hopsLabel
            // 
            this.hopsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.hopsLabel.AutoSize = true;
            this.hopsLabel.Location = new System.Drawing.Point(161, 10);
            this.hopsLabel.Name = "hopsLabel";
            this.hopsLabel.Size = new System.Drawing.Size(35, 13);
            this.hopsLabel.TabIndex = 29;
            this.hopsLabel.Text = "Hops:";
            this.toolTip1.SetToolTip(this.hopsLabel, "A hop is a station to load/unload from");
            // 
            // updateButton
            // 
            this.updateButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.updateButton.Location = new System.Drawing.Point(331, 713);
            this.updateButton.Name = "updateButton";
            this.updateButton.Size = new System.Drawing.Size(75, 23);
            this.updateButton.TabIndex = 24;
            this.updateButton.TabStop = false;
            this.updateButton.Text = "Update DB";
            this.toolTip1.SetToolTip(this.updateButton, "Update the database based on time from previous update");
            this.updateButton.UseVisualStyleBackColor = true;
            this.updateButton.Click += new System.EventHandler(this.updateButton_Click);
            // 
            // getSystemButton
            // 
            this.getSystemButton.Location = new System.Drawing.Point(320, 13);
            this.getSystemButton.Name = "getSystemButton";
            this.getSystemButton.Size = new System.Drawing.Size(21, 21);
            this.getSystemButton.TabIndex = 7;
            this.getSystemButton.TabStop = false;
            this.getSystemButton.Text = "C";
            this.toolTip1.SetToolTip(this.getSystemButton, "Get the most recent systems from the network logs (Ctrl+Click for a full refresh)" +
        "");
            this.getSystemButton.UseVisualStyleBackColor = true;
            this.getSystemButton.Click += new System.EventHandler(this.getSystemButton_Click);
            // 
            // srcSysLabel
            // 
            this.srcSysLabel.AutoSize = true;
            this.srcSysLabel.Location = new System.Drawing.Point(43, 16);
            this.srcSysLabel.Name = "srcSysLabel";
            this.srcSysLabel.Size = new System.Drawing.Size(44, 13);
            this.srcSysLabel.TabIndex = 2;
            this.srcSysLabel.Text = "Source:";
            this.toolTip1.SetToolTip(this.srcSysLabel, "Starting point in the form of system or system/station");
            // 
            // padSizeLabel
            // 
            this.padSizeLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.padSizeLabel.AutoSize = true;
            this.padSizeLabel.Location = new System.Drawing.Point(641, 15);
            this.padSizeLabel.Name = "padSizeLabel";
            this.padSizeLabel.Size = new System.Drawing.Size(52, 13);
            this.padSizeLabel.TabIndex = 34;
            this.padSizeLabel.Text = "Padsizes:";
            this.toolTip1.SetToolTip(this.padSizeLabel, "Minimum pad sizes to consider a hop, can be M, L, and/or ?");
            // 
            // padSizeBox
            // 
            this.padSizeBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.padSizeBox.Location = new System.Drawing.Point(693, 12);
            this.padSizeBox.MaxLength = 3;
            this.padSizeBox.Name = "padSizeBox";
            this.padSizeBox.Size = new System.Drawing.Size(32, 20);
            this.padSizeBox.TabIndex = 3;
            this.padSizeBox.Text = "?";
            this.padSizeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.padSizeBox, "Minimum pad sizes to consider a hop, can be M, L, and/or ?");
            this.padSizeBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.padSizeBox_KeyPress);
            // 
            // ageLabel
            // 
            this.ageLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ageLabel.AutoSize = true;
            this.ageLabel.Location = new System.Drawing.Point(554, 15);
            this.ageLabel.Name = "ageLabel";
            this.ageLabel.Size = new System.Drawing.Size(29, 13);
            this.ageLabel.TabIndex = 51;
            this.ageLabel.Text = "Age:";
            this.toolTip1.SetToolTip(this.ageLabel, "Filter any hops based on the age of their recent data, up to 30 days");
            // 
            // verboseLabel
            // 
            this.verboseLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.verboseLabel.AutoSize = true;
            this.verboseLabel.Location = new System.Drawing.Point(438, 16);
            this.verboseLabel.Name = "verboseLabel";
            this.verboseLabel.Size = new System.Drawing.Size(53, 13);
            this.verboseLabel.TabIndex = 55;
            this.verboseLabel.Text = "Verbosity:";
            this.toolTip1.SetToolTip(this.verboseLabel, "Verbosity of output results");
            // 
            // ageBox
            // 
            this.ageBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ageBox.Location = new System.Drawing.Point(583, 13);
            this.ageBox.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.ageBox.Name = "ageBox";
            this.ageBox.Size = new System.Drawing.Size(46, 20);
            this.ageBox.TabIndex = 2;
            this.ageBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.ageBox, "Filter any hops based on the age of their recent data, up to 30 days");
            this.ageBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.ageBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // editCommodButton
            // 
            this.editCommodButton.Location = new System.Drawing.Point(343, 13);
            this.editCommodButton.Name = "editCommodButton";
            this.editCommodButton.Size = new System.Drawing.Size(21, 21);
            this.editCommodButton.TabIndex = 8;
            this.editCommodButton.TabStop = false;
            this.editCommodButton.Text = "E";
            this.toolTip1.SetToolTip(this.editCommodButton, "Edit the commodities of an existing Source system/station (Ctrl+Click to produce " +
        "a zero\'d .prices file)");
            this.editCommodButton.UseVisualStyleBackColor = true;
            this.editCommodButton.Click += new System.EventHandler(this.editCommodButton_Click);
            // 
            // importButton
            // 
            this.importButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.importButton.Location = new System.Drawing.Point(487, 713);
            this.importButton.Name = "importButton";
            this.importButton.Size = new System.Drawing.Size(61, 23);
            this.importButton.TabIndex = 26;
            this.importButton.TabStop = false;
            this.importButton.Text = "Import Prices";
            this.toolTip1.SetToolTip(this.importButton, "Import prices from most recently docked station\r\n(Shift+Click for previous .price" +
        "s file)\r\n(Ctrl+Click for custom .prices file)");
            this.importButton.UseVisualStyleBackColor = true;
            this.importButton.Click += new System.EventHandler(this.importButton_Click);
            // 
            // gptLabel
            // 
            this.gptLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gptLabel.AutoSize = true;
            this.gptLabel.Location = new System.Drawing.Point(356, 79);
            this.gptLabel.Name = "gptLabel";
            this.gptLabel.Size = new System.Drawing.Size(52, 13);
            this.gptLabel.TabIndex = 27;
            this.gptLabel.Text = "Min GPT:";
            this.toolTip1.SetToolTip(this.gptLabel, "Minimum profit in credits per ton on any hop");
            // 
            // uploadButton
            // 
            this.uploadButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.uploadButton.Location = new System.Drawing.Point(554, 713);
            this.uploadButton.Name = "uploadButton";
            this.uploadButton.Size = new System.Drawing.Size(61, 23);
            this.uploadButton.TabIndex = 29;
            this.uploadButton.TabStop = false;
            this.uploadButton.Text = "Upload";
            this.toolTip1.SetToolTip(this.uploadButton, "Upload a chosen .prices file to Maddavo (Ctrl+Click to change path) [CAUTION!]");
            this.uploadButton.UseVisualStyleBackColor = true;
            this.uploadButton.Click += new System.EventHandler(this.uploadButton_Click);
            // 
            // lsFromStarLabel
            // 
            this.lsFromStarLabel.AutoSize = true;
            this.lsFromStarLabel.Location = new System.Drawing.Point(25, 12);
            this.lsFromStarLabel.Name = "lsFromStarLabel";
            this.lsFromStarLabel.Size = new System.Drawing.Size(68, 13);
            this.lsFromStarLabel.TabIndex = 0;
            this.lsFromStarLabel.Text = "LS from Star:";
            this.toolTip1.SetToolTip(this.lsFromStarLabel, "Number of light seconds between station and star");
            // 
            // maxPadSizeLabel
            // 
            this.maxPadSizeLabel.AutoSize = true;
            this.maxPadSizeLabel.Location = new System.Drawing.Point(203, 12);
            this.maxPadSizeLabel.Name = "maxPadSizeLabel";
            this.maxPadSizeLabel.Size = new System.Drawing.Size(70, 13);
            this.maxPadSizeLabel.TabIndex = 4;
            this.maxPadSizeLabel.Text = "Max Padsize:";
            this.toolTip1.SetToolTip(this.maxPadSizeLabel, "Maximum supported padsize (S/M/L/?)");
            // 
            // lsFromStarBox
            // 
            this.lsFromStarBox.Location = new System.Drawing.Point(99, 9);
            this.lsFromStarBox.Maximum = new decimal(new int[] {
            2000000,
            0,
            0,
            0});
            this.lsFromStarBox.Name = "lsFromStarBox";
            this.lsFromStarBox.Size = new System.Drawing.Size(80, 20);
            this.lsFromStarBox.TabIndex = 0;
            this.lsFromStarBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.lsFromStarBox.ThousandsSeparator = true;
            this.toolTip1.SetToolTip(this.lsFromStarBox, "Number of light seconds between station and star");
            this.lsFromStarBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.lsFromStarBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // blackMarketCheckBox
            // 
            this.blackMarketCheckBox.AutoSize = true;
            this.blackMarketCheckBox.Checked = true;
            this.blackMarketCheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.blackMarketCheckBox.Location = new System.Drawing.Point(93, 48);
            this.blackMarketCheckBox.Name = "blackMarketCheckBox";
            this.blackMarketCheckBox.Size = new System.Drawing.Size(89, 17);
            this.blackMarketCheckBox.TabIndex = 3;
            this.blackMarketCheckBox.TabStop = false;
            this.blackMarketCheckBox.Text = "Black Market";
            this.blackMarketCheckBox.ThreeState = true;
            this.toolTip1.SetToolTip(this.blackMarketCheckBox, "Does the station have a black market? (Default is unknown)");
            this.blackMarketCheckBox.UseVisualStyleBackColor = true;
            // 
            // marketCheckBox
            // 
            this.marketCheckBox.AutoSize = true;
            this.marketCheckBox.Checked = true;
            this.marketCheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.marketCheckBox.Location = new System.Drawing.Point(93, 26);
            this.marketCheckBox.Name = "marketCheckBox";
            this.marketCheckBox.Size = new System.Drawing.Size(85, 17);
            this.marketCheckBox.TabIndex = 4;
            this.marketCheckBox.TabStop = false;
            this.marketCheckBox.Text = "Commodities";
            this.marketCheckBox.ThreeState = true;
            this.toolTip1.SetToolTip(this.marketCheckBox, "Does the station have a commodities market? (Default is unknown)");
            this.marketCheckBox.UseVisualStyleBackColor = true;
            // 
            // shipyardCheckBox
            // 
            this.shipyardCheckBox.AutoSize = true;
            this.shipyardCheckBox.Checked = true;
            this.shipyardCheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.shipyardCheckBox.Location = new System.Drawing.Point(93, 4);
            this.shipyardCheckBox.Name = "shipyardCheckBox";
            this.shipyardCheckBox.Size = new System.Drawing.Size(67, 17);
            this.shipyardCheckBox.TabIndex = 2;
            this.shipyardCheckBox.TabStop = false;
            this.shipyardCheckBox.Text = "Shipyard";
            this.shipyardCheckBox.ThreeState = true;
            this.toolTip1.SetToolTip(this.shipyardCheckBox, "Does the station have a shipyard? (Default is unknown)");
            this.shipyardCheckBox.UseVisualStyleBackColor = true;
            // 
            // stn_padSizeBox
            // 
            this.stn_padSizeBox.Location = new System.Drawing.Point(278, 9);
            this.stn_padSizeBox.MaxLength = 1;
            this.stn_padSizeBox.Name = "stn_padSizeBox";
            this.stn_padSizeBox.Size = new System.Drawing.Size(27, 20);
            this.stn_padSizeBox.TabIndex = 1;
            this.stn_padSizeBox.Text = "?";
            this.stn_padSizeBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.toolTip1.SetToolTip(this.stn_padSizeBox, "Maximum supported padsize (S/M/L/?)");
            this.stn_padSizeBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stn_padSizeBox_KeyPress);
            // 
            // shipsSoldLabel
            // 
            this.shipsSoldLabel.AutoSize = true;
            this.shipsSoldLabel.Enabled = false;
            this.shipsSoldLabel.Location = new System.Drawing.Point(25, 136);
            this.shipsSoldLabel.Name = "shipsSoldLabel";
            this.shipsSoldLabel.Size = new System.Drawing.Size(60, 13);
            this.shipsSoldLabel.TabIndex = 12;
            this.shipsSoldLabel.Text = "Ships Sold:";
            this.toolTip1.SetToolTip(this.shipsSoldLabel, "Ships sold at this ship vendor (delimited by space or comma)");
            // 
            // srcSystemComboBox
            // 
            this.srcSystemComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.srcSystemComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.srcSystemComboBox.Location = new System.Drawing.Point(87, 13);
            this.srcSystemComboBox.Name = "srcSystemComboBox";
            this.srcSystemComboBox.Size = new System.Drawing.Size(230, 21);
            this.srcSystemComboBox.TabIndex = 0;
            this.toolTip1.SetToolTip(this.srcSystemComboBox, "Starting point in the form of system or system/station\r\nCtrl+Enter adds a System/" +
        "Station to the favorites\r\nShift+Enter removes a System/Station from the favorite" +
        "s");
            this.srcSystemComboBox.DropDown += new System.EventHandler(this.comboBox_DropDown);
            this.srcSystemComboBox.DropDownClosed += new System.EventHandler(this.comboBox_DropDownClosed);
            this.srcSystemComboBox.TextChanged += new System.EventHandler(this.srcSystemComboBox_TextChanged);
            this.srcSystemComboBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.srcSystemComboBox_KeyDown);
            // 
            // csvExportCheckBox
            // 
            this.csvExportCheckBox.AutoSize = true;
            this.csvExportCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.csvExportCheckBox.Location = new System.Drawing.Point(213, 37);
            this.csvExportCheckBox.Name = "csvExportCheckBox";
            this.csvExportCheckBox.Size = new System.Drawing.Size(92, 17);
            this.csvExportCheckBox.TabIndex = 14;
            this.csvExportCheckBox.TabStop = false;
            this.csvExportCheckBox.Text = "Export to CSV";
            this.toolTip1.SetToolTip(this.csvExportCheckBox, "(Override) Export all tables to CSV (volatile)");
            this.csvExportCheckBox.UseVisualStyleBackColor = true;
            this.csvExportCheckBox.CheckedChanged += new System.EventHandler(this.csvExportCheckBox_CheckedChanged);
            // 
            // csvExportComboBox
            // 
            this.csvExportComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.csvExportComboBox.FormattingEnabled = true;
            this.csvExportComboBox.Items.AddRange(new object[] {
            "",
            "System",
            "Station",
            "ShipVendor"});
            this.csvExportComboBox.Location = new System.Drawing.Point(218, 61);
            this.csvExportComboBox.Name = "csvExportComboBox";
            this.csvExportComboBox.Size = new System.Drawing.Size(87, 21);
            this.csvExportComboBox.TabIndex = 57;
            this.csvExportComboBox.TabStop = false;
            this.toolTip1.SetToolTip(this.csvExportComboBox, "Optional: Select a specific table to export to CSV");
            this.csvExportComboBox.SelectedIndexChanged += new System.EventHandler(this.csvExportComboBox_SelectedIndexChanged);
            // 
            // marginLabel
            // 
            this.marginLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.marginLabel.AutoSize = true;
            this.marginLabel.Location = new System.Drawing.Point(366, 125);
            this.marginLabel.Name = "marginLabel";
            this.marginLabel.Size = new System.Drawing.Size(42, 13);
            this.marginLabel.TabIndex = 53;
            this.marginLabel.Text = "Margin:";
            this.toolTip1.SetToolTip(this.marginLabel, "Profit margin variance");
            // 
            // marginBox
            // 
            this.marginBox.DecimalPlaces = 2;
            this.marginBox.Location = new System.Drawing.Point(409, 123);
            this.marginBox.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            131072});
            this.marginBox.Name = "marginBox";
            this.marginBox.Size = new System.Drawing.Size(60, 20);
            this.marginBox.TabIndex = 15;
            this.marginBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.marginBox, "Profit margin variance (<1.00)");
            this.marginBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.marginBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // limitLabel
            // 
            this.limitLabel.AutoSize = true;
            this.limitLabel.Location = new System.Drawing.Point(223, 102);
            this.limitLabel.Name = "limitLabel";
            this.limitLabel.Size = new System.Drawing.Size(62, 13);
            this.limitLabel.TabIndex = 13;
            this.limitLabel.Text = "Cargo Limit:";
            this.toolTip1.SetToolTip(this.limitLabel, "Limit each commodity purchased to this amount on a hop");
            // 
            // maxGPTLabel
            // 
            this.maxGPTLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.maxGPTLabel.AutoSize = true;
            this.maxGPTLabel.Location = new System.Drawing.Point(353, 102);
            this.maxGPTLabel.Name = "maxGPTLabel";
            this.maxGPTLabel.Size = new System.Drawing.Size(55, 13);
            this.maxGPTLabel.TabIndex = 55;
            this.maxGPTLabel.Text = "Max GPT:";
            this.toolTip1.SetToolTip(this.maxGPTLabel, "Maximum profit in credits per ton on any hop");
            // 
            // maxGPTBox
            // 
            this.maxGPTBox.Location = new System.Drawing.Point(409, 100);
            this.maxGPTBox.Maximum = new decimal(new int[] {
            20000,
            0,
            0,
            0});
            this.maxGPTBox.Name = "maxGPTBox";
            this.maxGPTBox.Size = new System.Drawing.Size(60, 20);
            this.maxGPTBox.TabIndex = 12;
            this.maxGPTBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.maxGPTBox.ThousandsSeparator = true;
            this.toolTip1.SetToolTip(this.maxGPTBox, "Maximum profit in credits per ton on any hop");
            this.maxGPTBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.maxGPTBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // rearmCheckBox
            // 
            this.rearmCheckBox.AutoSize = true;
            this.rearmCheckBox.Checked = true;
            this.rearmCheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.rearmCheckBox.Location = new System.Drawing.Point(5, 4);
            this.rearmCheckBox.Name = "rearmCheckBox";
            this.rearmCheckBox.Size = new System.Drawing.Size(57, 17);
            this.rearmCheckBox.TabIndex = 5;
            this.rearmCheckBox.TabStop = false;
            this.rearmCheckBox.Text = "Rearm";
            this.rearmCheckBox.ThreeState = true;
            this.toolTip1.SetToolTip(this.rearmCheckBox, "Does the station have rearm facilities? (Default is unknown)");
            this.rearmCheckBox.UseVisualStyleBackColor = true;
            // 
            // repairCheckBox
            // 
            this.repairCheckBox.AutoSize = true;
            this.repairCheckBox.Checked = true;
            this.repairCheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.repairCheckBox.Location = new System.Drawing.Point(5, 48);
            this.repairCheckBox.Name = "repairCheckBox";
            this.repairCheckBox.Size = new System.Drawing.Size(57, 17);
            this.repairCheckBox.TabIndex = 7;
            this.repairCheckBox.TabStop = false;
            this.repairCheckBox.Text = "Repair";
            this.repairCheckBox.ThreeState = true;
            this.toolTip1.SetToolTip(this.repairCheckBox, "Does the station have repair facilities? (Default is unknown)");
            this.repairCheckBox.UseVisualStyleBackColor = true;
            // 
            // refuelCheckBox
            // 
            this.refuelCheckBox.AutoSize = true;
            this.refuelCheckBox.Checked = true;
            this.refuelCheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.refuelCheckBox.Location = new System.Drawing.Point(5, 26);
            this.refuelCheckBox.Name = "refuelCheckBox";
            this.refuelCheckBox.Size = new System.Drawing.Size(57, 17);
            this.refuelCheckBox.TabIndex = 6;
            this.refuelCheckBox.TabStop = false;
            this.refuelCheckBox.Text = "Refuel";
            this.refuelCheckBox.ThreeState = true;
            this.toolTip1.SetToolTip(this.refuelCheckBox, "Does the station have refueling facilities? (Default is unknown)");
            this.refuelCheckBox.UseVisualStyleBackColor = true;
            // 
            // outfitCheckBox
            // 
            this.outfitCheckBox.AutoSize = true;
            this.outfitCheckBox.Checked = true;
            this.outfitCheckBox.CheckState = System.Windows.Forms.CheckState.Indeterminate;
            this.outfitCheckBox.Location = new System.Drawing.Point(5, 70);
            this.outfitCheckBox.Name = "outfitCheckBox";
            this.outfitCheckBox.Size = new System.Drawing.Size(68, 17);
            this.outfitCheckBox.TabIndex = 58;
            this.outfitCheckBox.TabStop = false;
            this.outfitCheckBox.Text = "Outfitting";
            this.outfitCheckBox.ThreeState = true;
            this.toolTip1.SetToolTip(this.outfitCheckBox, "Does the station have outfitting facilities? (Default is unknown)");
            this.outfitCheckBox.UseVisualStyleBackColor = true;
            // 
            // outfitFilterCheckBox
            // 
            this.outfitFilterCheckBox.AutoSize = true;
            this.outfitFilterCheckBox.Location = new System.Drawing.Point(9, 99);
            this.outfitFilterCheckBox.Name = "outfitFilterCheckBox";
            this.outfitFilterCheckBox.Size = new System.Drawing.Size(68, 17);
            this.outfitFilterCheckBox.TabIndex = 58;
            this.outfitFilterCheckBox.TabStop = false;
            this.outfitFilterCheckBox.Text = "Outfitting";
            this.toolTip1.SetToolTip(this.outfitFilterCheckBox, "Does the station have outfitting facilities? (Default is unknown)");
            this.outfitFilterCheckBox.UseVisualStyleBackColor = true;
            // 
            // rearmFilterCheckBox
            // 
            this.rearmFilterCheckBox.AutoSize = true;
            this.rearmFilterCheckBox.Location = new System.Drawing.Point(9, 21);
            this.rearmFilterCheckBox.Name = "rearmFilterCheckBox";
            this.rearmFilterCheckBox.Size = new System.Drawing.Size(57, 17);
            this.rearmFilterCheckBox.TabIndex = 5;
            this.rearmFilterCheckBox.TabStop = false;
            this.rearmFilterCheckBox.Text = "Rearm";
            this.toolTip1.SetToolTip(this.rearmFilterCheckBox, "Does the station have rearm facilities? (Default is unknown)");
            this.rearmFilterCheckBox.UseVisualStyleBackColor = true;
            // 
            // repairFilterCheckBox
            // 
            this.repairFilterCheckBox.AutoSize = true;
            this.repairFilterCheckBox.Location = new System.Drawing.Point(9, 73);
            this.repairFilterCheckBox.Name = "repairFilterCheckBox";
            this.repairFilterCheckBox.Size = new System.Drawing.Size(57, 17);
            this.repairFilterCheckBox.TabIndex = 7;
            this.repairFilterCheckBox.TabStop = false;
            this.repairFilterCheckBox.Text = "Repair";
            this.toolTip1.SetToolTip(this.repairFilterCheckBox, "Does the station have repair facilities? (Default is unknown)");
            this.repairFilterCheckBox.UseVisualStyleBackColor = true;
            // 
            // refuelFilterCheckBox
            // 
            this.refuelFilterCheckBox.AutoSize = true;
            this.refuelFilterCheckBox.Location = new System.Drawing.Point(9, 47);
            this.refuelFilterCheckBox.Name = "refuelFilterCheckBox";
            this.refuelFilterCheckBox.Size = new System.Drawing.Size(57, 17);
            this.refuelFilterCheckBox.TabIndex = 6;
            this.refuelFilterCheckBox.TabStop = false;
            this.refuelFilterCheckBox.Text = "Refuel";
            this.toolTip1.SetToolTip(this.refuelFilterCheckBox, "Does the station have refueling facilities? (Default is unknown)");
            this.refuelFilterCheckBox.UseVisualStyleBackColor = true;
            // 
            // shipyardFilterCheckBox
            // 
            this.shipyardFilterCheckBox.AutoSize = true;
            this.shipyardFilterCheckBox.Location = new System.Drawing.Point(72, 21);
            this.shipyardFilterCheckBox.Name = "shipyardFilterCheckBox";
            this.shipyardFilterCheckBox.Size = new System.Drawing.Size(67, 17);
            this.shipyardFilterCheckBox.TabIndex = 2;
            this.shipyardFilterCheckBox.TabStop = false;
            this.shipyardFilterCheckBox.Text = "Shipyard";
            this.toolTip1.SetToolTip(this.shipyardFilterCheckBox, "Does the station have a shipyard? (Default is unknown)");
            this.shipyardFilterCheckBox.UseVisualStyleBackColor = true;
            // 
            // bmktFilterCheckBox
            // 
            this.bmktFilterCheckBox.AutoSize = true;
            this.bmktFilterCheckBox.Location = new System.Drawing.Point(72, 73);
            this.bmktFilterCheckBox.Name = "bmktFilterCheckBox";
            this.bmktFilterCheckBox.Size = new System.Drawing.Size(89, 17);
            this.bmktFilterCheckBox.TabIndex = 3;
            this.bmktFilterCheckBox.TabStop = false;
            this.bmktFilterCheckBox.Text = "Black Market";
            this.toolTip1.SetToolTip(this.bmktFilterCheckBox, "Does the station have a black market? (Default is unknown)");
            this.bmktFilterCheckBox.UseVisualStyleBackColor = true;
            // 
            // itemsFilterCheckBox
            // 
            this.itemsFilterCheckBox.AutoSize = true;
            this.itemsFilterCheckBox.Location = new System.Drawing.Point(72, 47);
            this.itemsFilterCheckBox.Name = "itemsFilterCheckBox";
            this.itemsFilterCheckBox.Size = new System.Drawing.Size(85, 17);
            this.itemsFilterCheckBox.TabIndex = 4;
            this.itemsFilterCheckBox.TabStop = false;
            this.itemsFilterCheckBox.Text = "Commodities";
            this.toolTip1.SetToolTip(this.itemsFilterCheckBox, "Does the station have a commodities market? (Default is unknown)");
            this.itemsFilterCheckBox.UseVisualStyleBackColor = true;
            // 
            // forceUpdateBox
            // 
            this.forceUpdateBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.forceUpdateBox.DisplayMember = "ShipVendors";
            this.forceUpdateBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.forceUpdateBox.Items.AddRange(new object[] {
            "None",
            "All",
            "Stations",
            "Systems",
            "ShipVendors",
            "Force Prices",
            "Recache"});
            this.forceUpdateBox.Location = new System.Drawing.Point(203, 714);
            this.forceUpdateBox.Name = "forceUpdateBox";
            this.forceUpdateBox.Size = new System.Drawing.Size(88, 21);
            this.forceUpdateBox.TabIndex = 61;
            this.forceUpdateBox.TabStop = false;
            this.toolTip1.SetToolTip(this.forceUpdateBox, "Includes additional CSVs during the update process");
            this.forceUpdateBox.SelectedIndexChanged += new System.EventHandler(this.forceUpdateBox_SelectedIndexChanged);
            // 
            // skipImportCheckBox
            // 
            this.skipImportCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.skipImportCheckBox.AutoSize = true;
            this.skipImportCheckBox.Location = new System.Drawing.Point(412, 718);
            this.skipImportCheckBox.Name = "skipImportCheckBox";
            this.skipImportCheckBox.Size = new System.Drawing.Size(15, 14);
            this.skipImportCheckBox.TabIndex = 62;
            this.skipImportCheckBox.TabStop = false;
            this.toolTip1.SetToolTip(this.skipImportCheckBox, "Skip importing of prices, and force downloading of CSVs");
            this.skipImportCheckBox.UseVisualStyleBackColor = true;
            this.skipImportCheckBox.CheckedChanged += new System.EventHandler(this.skipImportCheckBox_CheckedChanged);
            // 
            // resetFilterButton
            // 
            this.resetFilterButton.Location = new System.Drawing.Point(142, 96);
            this.resetFilterButton.Name = "resetFilterButton";
            this.resetFilterButton.Size = new System.Drawing.Size(19, 20);
            this.resetFilterButton.TabIndex = 59;
            this.resetFilterButton.TabStop = false;
            this.resetFilterButton.Text = "R";
            this.toolTip1.SetToolTip(this.resetFilterButton, "Reset filters");
            this.resetFilterButton.UseVisualStyleBackColor = true;
            this.resetFilterButton.Click += new System.EventHandler(this.resetFilterButton_Click);
            // 
            // resetStationButton
            // 
            this.resetStationButton.Location = new System.Drawing.Point(286, 112);
            this.resetStationButton.Name = "resetStationButton";
            this.resetStationButton.Size = new System.Drawing.Size(19, 20);
            this.resetStationButton.TabIndex = 60;
            this.resetStationButton.TabStop = false;
            this.resetStationButton.Text = "R";
            this.toolTip1.SetToolTip(this.resetStationButton, "Reset");
            this.resetStationButton.UseVisualStyleBackColor = true;
            this.resetStationButton.Click += new System.EventHandler(this.resetStationButton_Click);
            // 
            // loadSettingsButton
            // 
            this.loadSettingsButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.loadSettingsButton.Location = new System.Drawing.Point(642, 713);
            this.loadSettingsButton.Name = "loadSettingsButton";
            this.loadSettingsButton.Size = new System.Drawing.Size(83, 23);
            this.loadSettingsButton.TabIndex = 27;
            this.loadSettingsButton.TabStop = false;
            this.loadSettingsButton.Text = "Load Settings";
            this.toolTip1.SetToolTip(this.loadSettingsButton, "Ctrl+Click to choose a different settings file to load");
            this.loadSettingsButton.UseVisualStyleBackColor = true;
            this.loadSettingsButton.Click += new System.EventHandler(this.loadSettingsButton_Click);
            // 
            // confirmBox
            // 
            this.confirmBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.confirmBox.Location = new System.Drawing.Point(141, 68);
            this.confirmBox.MaxLength = 4;
            this.confirmBox.Name = "confirmBox";
            this.confirmBox.Size = new System.Drawing.Size(35, 20);
            this.confirmBox.TabIndex = 59;
            this.toolTip1.SetToolTip(this.confirmBox, "4 character Hex confirm code");
            // 
            // swapButton
            // 
            this.swapButton.Location = new System.Drawing.Point(12, 12);
            this.swapButton.Name = "swapButton";
            this.swapButton.Size = new System.Drawing.Size(21, 21);
            this.swapButton.TabIndex = 63;
            this.swapButton.TabStop = false;
            this.swapButton.Text = "S";
            this.toolTip1.SetToolTip(this.swapButton, "Swap the contents of Source/Destination");
            this.swapButton.UseVisualStyleBackColor = true;
            this.swapButton.Click += new System.EventHandler(this.swapButton_Click);
            // 
            // miniModeButton
            // 
            this.miniModeButton.Enabled = false;
            this.miniModeButton.Location = new System.Drawing.Point(366, 13);
            this.miniModeButton.Name = "miniModeButton";
            this.miniModeButton.Size = new System.Drawing.Size(21, 21);
            this.miniModeButton.TabIndex = 1;
            this.miniModeButton.TabStop = false;
            this.miniModeButton.Text = "&T";
            this.toolTip1.SetToolTip(this.miniModeButton, "Switch to a minimal TreeView mode for Run output (ESC to exit)");
            this.miniModeButton.UseVisualStyleBackColor = true;
            this.miniModeButton.Click += new System.EventHandler(this.miniModeButton_Click);
            // 
            // altConfigBox
            // 
            this.altConfigBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.altConfigBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.altConfigBox.Location = new System.Drawing.Point(350, 43);
            this.altConfigBox.Name = "altConfigBox";
            this.altConfigBox.Size = new System.Drawing.Size(82, 21);
            this.altConfigBox.TabIndex = 56;
            this.altConfigBox.TabStop = false;
            this.toolTip1.SetToolTip(this.altConfigBox, "Select a previously used config file\r\n(Ctrl+Click a non-default config to delete " +
        "it)");
            this.altConfigBox.DropDown += new System.EventHandler(this.altConfigBox_DropDown);
            this.altConfigBox.SelectionChangeCommitted += new System.EventHandler(this.altConfigBox_SelectionChangeCommitted);
            this.altConfigBox.Click += new System.EventHandler(this.altConfigBox_Click);
            // 
            // correctCheckBox
            // 
            this.correctCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.correctCheckBox.AutoSize = true;
            this.correctCheckBox.Location = new System.Drawing.Point(297, 718);
            this.correctCheckBox.Name = "correctCheckBox";
            this.correctCheckBox.Size = new System.Drawing.Size(15, 14);
            this.correctCheckBox.TabIndex = 64;
            this.correctCheckBox.TabStop = false;
            this.toolTip1.SetToolTip(this.correctCheckBox, "Force the downloading of Corrections with prices data");
            this.correctCheckBox.UseVisualStyleBackColor = true;
            this.correctCheckBox.CheckedChanged += new System.EventHandler(this.correctCheckBox_CheckedChanged);
            // 
            // updateNotifyIcon
            // 
            this.updateNotifyIcon.Image = global::TDHelper.Properties.Resources.LightningBolt;
            this.updateNotifyIcon.Location = new System.Drawing.Point(9, 140);
            this.updateNotifyIcon.Name = "updateNotifyIcon";
            this.updateNotifyIcon.Size = new System.Drawing.Size(16, 16);
            this.updateNotifyIcon.TabIndex = 59;
            this.updateNotifyIcon.TabStop = false;
            this.toolTip1.SetToolTip(this.updateNotifyIcon, "An update to TDHelper is available! Restart to update.");
            this.updateNotifyIcon.Visible = false;
            // 
            // stockBox
            // 
            this.stockBox.Location = new System.Drawing.Point(270, 77);
            this.stockBox.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.stockBox.Name = "stockBox";
            this.stockBox.Size = new System.Drawing.Size(71, 20);
            this.stockBox.TabIndex = 8;
            this.stockBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.stockBox.ThousandsSeparator = true;
            this.toolTip1.SetToolTip(this.stockBox, "Filter hops below this level of stock");
            this.stockBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.stockBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // stockLabel
            // 
            this.stockLabel.AutoSize = true;
            this.stockLabel.Location = new System.Drawing.Point(219, 79);
            this.stockLabel.Name = "stockLabel";
            this.stockLabel.Size = new System.Drawing.Size(50, 13);
            this.stockLabel.TabIndex = 57;
            this.stockLabel.Text = "    Stock:";
            this.toolTip1.SetToolTip(this.stockLabel, "Filter hops below this level of stock");
            // 
            // testSystemsCheckBox
            // 
            this.testSystemsCheckBox.AutoSize = true;
            this.testSystemsCheckBox.Location = new System.Drawing.Point(391, 16);
            this.testSystemsCheckBox.Name = "testSystemsCheckBox";
            this.testSystemsCheckBox.Size = new System.Drawing.Size(15, 14);
            this.testSystemsCheckBox.TabIndex = 58;
            this.testSystemsCheckBox.TabStop = false;
            this.toolTip1.SetToolTip(this.testSystemsCheckBox, "Notify when entering unknown systems");
            this.testSystemsCheckBox.UseVisualStyleBackColor = true;
            this.testSystemsCheckBox.CheckedChanged += new System.EventHandler(this.testSystemsCheckBox_CheckedChanged);
            // 
            // stationsFilterCheckBox
            // 
            this.stationsFilterCheckBox.AutoSize = true;
            this.stationsFilterCheckBox.Location = new System.Drawing.Point(121, 99);
            this.stationsFilterCheckBox.Name = "stationsFilterCheckBox";
            this.stationsFilterCheckBox.Size = new System.Drawing.Size(15, 14);
            this.stationsFilterCheckBox.TabIndex = 60;
            this.toolTip1.SetToolTip(this.stationsFilterCheckBox, "Filter destinations that don\'t contain stations");
            this.stationsFilterCheckBox.UseVisualStyleBackColor = true;
            // 
            // minAgeUpDown
            // 
            this.minAgeUpDown.Location = new System.Drawing.Point(265, 86);
            this.minAgeUpDown.Maximum = new decimal(new int[] {
            365,
            0,
            0,
            0});
            this.minAgeUpDown.Name = "minAgeUpDown";
            this.minAgeUpDown.Size = new System.Drawing.Size(40, 20);
            this.minAgeUpDown.TabIndex = 61;
            this.toolTip1.SetToolTip(this.minAgeUpDown, "Minimum age in days to filter by");
            this.minAgeUpDown.Visible = false;
            this.minAgeUpDown.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.minAgeUpDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // minAgeLabel
            // 
            this.minAgeLabel.AutoSize = true;
            this.minAgeLabel.Location = new System.Drawing.Point(215, 88);
            this.minAgeLabel.Name = "minAgeLabel";
            this.minAgeLabel.Size = new System.Drawing.Size(46, 13);
            this.minAgeLabel.TabIndex = 62;
            this.minAgeLabel.Text = "MinAge:";
            this.toolTip1.SetToolTip(this.minAgeLabel, "Minimum age in days to filter by");
            this.minAgeLabel.Visible = false;
            // 
            // oldRoutesCheckBox
            // 
            this.oldRoutesCheckBox.AutoSize = true;
            this.oldRoutesCheckBox.Location = new System.Drawing.Point(218, 115);
            this.oldRoutesCheckBox.Name = "oldRoutesCheckBox";
            this.oldRoutesCheckBox.Size = new System.Drawing.Size(55, 17);
            this.oldRoutesCheckBox.TabIndex = 63;
            this.oldRoutesCheckBox.Text = "Route";
            this.toolTip1.SetToolTip(this.oldRoutesCheckBox, "Sort the results of OldData into a route by distance");
            this.oldRoutesCheckBox.UseVisualStyleBackColor = true;
            this.oldRoutesCheckBox.Visible = false;
            // 
            // edscLYLabel5
            // 
            this.edscLYLabel5.AutoSize = true;
            this.edscLYLabel5.Location = new System.Drawing.Point(221, 102);
            this.edscLYLabel5.Name = "edscLYLabel5";
            this.edscLYLabel5.Size = new System.Drawing.Size(23, 13);
            this.edscLYLabel5.TabIndex = 23;
            this.edscLYLabel5.Text = "LY:";
            this.toolTip1.SetToolTip(this.edscLYLabel5, "Distance in LY (required for a submission)");
            // 
            // edscLYLabel4
            // 
            this.edscLYLabel4.AutoSize = true;
            this.edscLYLabel4.Location = new System.Drawing.Point(221, 79);
            this.edscLYLabel4.Name = "edscLYLabel4";
            this.edscLYLabel4.Size = new System.Drawing.Size(23, 13);
            this.edscLYLabel4.TabIndex = 22;
            this.edscLYLabel4.Text = "LY:";
            this.toolTip1.SetToolTip(this.edscLYLabel4, "Distance in LY (required for a submission)");
            // 
            // edscLYLabel3
            // 
            this.edscLYLabel3.AutoSize = true;
            this.edscLYLabel3.Location = new System.Drawing.Point(221, 56);
            this.edscLYLabel3.Name = "edscLYLabel3";
            this.edscLYLabel3.Size = new System.Drawing.Size(23, 13);
            this.edscLYLabel3.TabIndex = 21;
            this.edscLYLabel3.Text = "LY:";
            this.toolTip1.SetToolTip(this.edscLYLabel3, "Distance in LY (required for a submission)");
            // 
            // edscLYLabel2
            // 
            this.edscLYLabel2.AutoSize = true;
            this.edscLYLabel2.Location = new System.Drawing.Point(221, 33);
            this.edscLYLabel2.Name = "edscLYLabel2";
            this.edscLYLabel2.Size = new System.Drawing.Size(23, 13);
            this.edscLYLabel2.TabIndex = 20;
            this.edscLYLabel2.Text = "LY:";
            this.toolTip1.SetToolTip(this.edscLYLabel2, "Distance in LY (required for a submission)");
            // 
            // refSysTextBox5
            // 
            this.refSysTextBox5.Location = new System.Drawing.Point(75, 100);
            this.refSysTextBox5.Name = "refSysTextBox5";
            this.refSysTextBox5.Size = new System.Drawing.Size(140, 20);
            this.refSysTextBox5.TabIndex = 9;
            this.toolTip1.SetToolTip(this.refSysTextBox5, "All 5 references (with distances) are required for a submission");
            // 
            // refSysLabel5
            // 
            this.refSysLabel5.AutoSize = true;
            this.refSysLabel5.Location = new System.Drawing.Point(7, 102);
            this.refSysLabel5.Name = "refSysLabel5";
            this.refSysLabel5.Size = new System.Drawing.Size(69, 13);
            this.refSysLabel5.TabIndex = 16;
            this.refSysLabel5.Text = "Ref. Sys. #5:";
            this.toolTip1.SetToolTip(this.refSysLabel5, "All 5 references (with distances) are required for a submission");
            // 
            // refSysTextBox4
            // 
            this.refSysTextBox4.Location = new System.Drawing.Point(75, 77);
            this.refSysTextBox4.Name = "refSysTextBox4";
            this.refSysTextBox4.Size = new System.Drawing.Size(140, 20);
            this.refSysTextBox4.TabIndex = 7;
            this.toolTip1.SetToolTip(this.refSysTextBox4, "All 5 references (with distances) are required for a submission");
            // 
            // refSysLabel4
            // 
            this.refSysLabel4.AutoSize = true;
            this.refSysLabel4.Location = new System.Drawing.Point(7, 79);
            this.refSysLabel4.Name = "refSysLabel4";
            this.refSysLabel4.Size = new System.Drawing.Size(69, 13);
            this.refSysLabel4.TabIndex = 12;
            this.refSysLabel4.Text = "Ref. Sys. #4:";
            this.toolTip1.SetToolTip(this.refSysLabel4, "All 5 references (with distances) are required for a submission");
            // 
            // refSysTextBox3
            // 
            this.refSysTextBox3.Location = new System.Drawing.Point(75, 54);
            this.refSysTextBox3.Name = "refSysTextBox3";
            this.refSysTextBox3.Size = new System.Drawing.Size(140, 20);
            this.refSysTextBox3.TabIndex = 5;
            this.toolTip1.SetToolTip(this.refSysTextBox3, "All 5 references (with distances) are required for a submission");
            // 
            // refSysLabel3
            // 
            this.refSysLabel3.AutoSize = true;
            this.refSysLabel3.Location = new System.Drawing.Point(7, 56);
            this.refSysLabel3.Name = "refSysLabel3";
            this.refSysLabel3.Size = new System.Drawing.Size(69, 13);
            this.refSysLabel3.TabIndex = 8;
            this.refSysLabel3.Text = "Ref. Sys. #3:";
            this.toolTip1.SetToolTip(this.refSysLabel3, "All 5 references (with distances) are required for a submission");
            // 
            // refSysTextBox2
            // 
            this.refSysTextBox2.Location = new System.Drawing.Point(75, 31);
            this.refSysTextBox2.Name = "refSysTextBox2";
            this.refSysTextBox2.Size = new System.Drawing.Size(140, 20);
            this.refSysTextBox2.TabIndex = 3;
            this.toolTip1.SetToolTip(this.refSysTextBox2, "All 5 references (with distances) are required for a submission");
            // 
            // refSysLabel2
            // 
            this.refSysLabel2.AutoSize = true;
            this.refSysLabel2.Location = new System.Drawing.Point(7, 33);
            this.refSysLabel2.Name = "refSysLabel2";
            this.refSysLabel2.Size = new System.Drawing.Size(69, 13);
            this.refSysLabel2.TabIndex = 4;
            this.refSysLabel2.Text = "Ref. Sys. #2:";
            this.toolTip1.SetToolTip(this.refSysLabel2, "All 5 references (with distances) are required for a submission");
            // 
            // edscLYLabel1
            // 
            this.edscLYLabel1.AutoSize = true;
            this.edscLYLabel1.Location = new System.Drawing.Point(221, 10);
            this.edscLYLabel1.Name = "edscLYLabel1";
            this.edscLYLabel1.Size = new System.Drawing.Size(23, 13);
            this.edscLYLabel1.TabIndex = 2;
            this.edscLYLabel1.Text = "LY:";
            this.toolTip1.SetToolTip(this.edscLYLabel1, "Distance in LY (required for a submission)");
            // 
            // refSysTextBox1
            // 
            this.refSysTextBox1.Location = new System.Drawing.Point(75, 8);
            this.refSysTextBox1.Name = "refSysTextBox1";
            this.refSysTextBox1.Size = new System.Drawing.Size(140, 20);
            this.refSysTextBox1.TabIndex = 1;
            this.toolTip1.SetToolTip(this.refSysTextBox1, "All 5 references (with distances) are required for a submission");
            // 
            // refSysLabel1
            // 
            this.refSysLabel1.AutoSize = true;
            this.refSysLabel1.Location = new System.Drawing.Point(7, 10);
            this.refSysLabel1.Name = "refSysLabel1";
            this.refSysLabel1.Size = new System.Drawing.Size(69, 13);
            this.refSysLabel1.TabIndex = 0;
            this.refSysLabel1.Text = "Ref. Sys. #1:";
            this.toolTip1.SetToolTip(this.refSysLabel1, "All 5 references (with distances) are required for a submission");
            // 
            // cmdrNameLabel
            // 
            this.cmdrNameLabel.AutoSize = true;
            this.cmdrNameLabel.Location = new System.Drawing.Point(7, 126);
            this.cmdrNameLabel.Name = "cmdrNameLabel";
            this.cmdrNameLabel.Size = new System.Drawing.Size(34, 13);
            this.cmdrNameLabel.TabIndex = 24;
            this.cmdrNameLabel.Text = "Cmdr:";
            this.toolTip1.SetToolTip(this.cmdrNameLabel, "Optional: Specify name of submitter on entry");
            // 
            // cmdrNameTextBox
            // 
            this.cmdrNameTextBox.Location = new System.Drawing.Point(40, 123);
            this.cmdrNameTextBox.Name = "cmdrNameTextBox";
            this.cmdrNameTextBox.Size = new System.Drawing.Size(175, 20);
            this.cmdrNameTextBox.TabIndex = 11;
            this.toolTip1.SetToolTip(this.cmdrNameTextBox, "Optional: Specify name of submitter on entry");
            // 
            // loopIntLabel
            // 
            this.loopIntLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.loopIntLabel.AutoSize = true;
            this.loopIntLabel.Location = new System.Drawing.Point(113, 102);
            this.loopIntLabel.Name = "loopIntLabel";
            this.loopIntLabel.Size = new System.Drawing.Size(49, 13);
            this.loopIntLabel.TabIndex = 59;
            this.loopIntLabel.Text = "Loop Int:";
            this.toolTip1.SetToolTip(this.loopIntLabel, "Minimum hops between visiting the same station");
            // 
            // loopIntBox
            // 
            this.loopIntBox.Location = new System.Drawing.Point(163, 100);
            this.loopIntBox.Name = "loopIntBox";
            this.loopIntBox.Size = new System.Drawing.Size(55, 20);
            this.loopIntBox.TabIndex = 10;
            this.loopIntBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.loopIntBox, "Minimum hops between visiting the same station");
            this.loopIntBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.loopIntBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // edscLYBox1
            // 
            this.edscLYBox1.DecimalPlaces = 2;
            this.edscLYBox1.Location = new System.Drawing.Point(245, 8);
            this.edscLYBox1.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.edscLYBox1.Name = "edscLYBox1";
            this.edscLYBox1.Size = new System.Drawing.Size(60, 20);
            this.edscLYBox1.TabIndex = 2;
            this.edscLYBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.edscLYBox1, "Distance in LY (required for a submission)");
            this.edscLYBox1.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.edscLYBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // edscLYBox2
            // 
            this.edscLYBox2.DecimalPlaces = 2;
            this.edscLYBox2.Location = new System.Drawing.Point(245, 31);
            this.edscLYBox2.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.edscLYBox2.Name = "edscLYBox2";
            this.edscLYBox2.Size = new System.Drawing.Size(60, 20);
            this.edscLYBox2.TabIndex = 4;
            this.edscLYBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.edscLYBox2, "Distance in LY (required for a submission)");
            this.edscLYBox2.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.edscLYBox2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // edscLYBox3
            // 
            this.edscLYBox3.DecimalPlaces = 2;
            this.edscLYBox3.Location = new System.Drawing.Point(245, 54);
            this.edscLYBox3.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.edscLYBox3.Name = "edscLYBox3";
            this.edscLYBox3.Size = new System.Drawing.Size(60, 20);
            this.edscLYBox3.TabIndex = 6;
            this.edscLYBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.edscLYBox3, "Distance in LY (required for a submission)");
            this.edscLYBox3.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.edscLYBox3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // edscLYBox4
            // 
            this.edscLYBox4.DecimalPlaces = 2;
            this.edscLYBox4.Location = new System.Drawing.Point(245, 77);
            this.edscLYBox4.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.edscLYBox4.Name = "edscLYBox4";
            this.edscLYBox4.Size = new System.Drawing.Size(60, 20);
            this.edscLYBox4.TabIndex = 8;
            this.edscLYBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.edscLYBox4, "Distance in LY (required for a submission)");
            this.edscLYBox4.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.edscLYBox4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // edscLYBox5
            // 
            this.edscLYBox5.DecimalPlaces = 2;
            this.edscLYBox5.Location = new System.Drawing.Point(245, 100);
            this.edscLYBox5.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.edscLYBox5.Name = "edscLYBox5";
            this.edscLYBox5.Size = new System.Drawing.Size(60, 20);
            this.edscLYBox5.TabIndex = 10;
            this.edscLYBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.edscLYBox5, "Distance in LY (required for a submission)");
            this.edscLYBox5.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.edscLYBox5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // shortenCheckBox
            // 
            this.shortenCheckBox.AutoSize = true;
            this.shortenCheckBox.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.shortenCheckBox.Location = new System.Drawing.Point(111, 32);
            this.shortenCheckBox.Name = "shortenCheckBox";
            this.shortenCheckBox.Size = new System.Drawing.Size(63, 17);
            this.shortenCheckBox.TabIndex = 61;
            this.shortenCheckBox.TabStop = false;
            this.shortenCheckBox.Text = "Shorten";
            this.toolTip1.SetToolTip(this.shortenCheckBox, "Finds the highest gainful route with the least hops (requires a Destination)");
            this.shortenCheckBox.UseVisualStyleBackColor = true;
            this.shortenCheckBox.Click += new System.EventHandler(this.shortenCheckBox_Click);
            // 
            // shipsSoldBox
            // 
            this.shipsSoldBox.Location = new System.Drawing.Point(91, 133);
            this.shipsSoldBox.Name = "shipsSoldBox";
            this.shipsSoldBox.Size = new System.Drawing.Size(214, 21);
            this.shipsSoldBox.TabIndex = 8;
            this.toolTip1.SetToolTip(this.shipsSoldBox, "Ships sold at this ship vendor (delimited by space or comma)");
            // 
            // crFilterLabel
            // 
            this.crFilterLabel.AutoSize = true;
            this.crFilterLabel.Location = new System.Drawing.Point(219, 126);
            this.crFilterLabel.Name = "crFilterLabel";
            this.crFilterLabel.Size = new System.Drawing.Size(25, 13);
            this.crFilterLabel.TabIndex = 25;
            this.crFilterLabel.Text = "CR:";
            this.toolTip1.SetToolTip(this.crFilterLabel, "Filter by credit rating");
            // 
            // crFilterUpDown
            // 
            this.crFilterUpDown.Location = new System.Drawing.Point(245, 123);
            this.crFilterUpDown.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.crFilterUpDown.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.crFilterUpDown.Name = "crFilterUpDown";
            this.crFilterUpDown.Size = new System.Drawing.Size(60, 20);
            this.crFilterUpDown.TabIndex = 12;
            this.crFilterUpDown.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.toolTip1.SetToolTip(this.crFilterUpDown, "Filter by credit rating");
            this.crFilterUpDown.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.crFilterUpDown.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.crFilterUpDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // demandBox
            // 
            this.demandBox.Location = new System.Drawing.Point(147, 77);
            this.demandBox.Maximum = new decimal(new int[] {
            9999999,
            0,
            0,
            0});
            this.demandBox.Name = "demandBox";
            this.demandBox.Size = new System.Drawing.Size(71, 20);
            this.demandBox.TabIndex = 7;
            this.demandBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.demandBox.ThousandsSeparator = true;
            this.toolTip1.SetToolTip(this.demandBox, "Filter hops below this level of demand");
            this.demandBox.Enter += new System.EventHandler(this.numericUpDown_Enter);
            this.demandBox.MouseUp += new System.Windows.Forms.MouseEventHandler(this.numericUpDown_MouseUp);
            // 
            // demandLabel
            // 
            this.demandLabel.AutoSize = true;
            this.demandLabel.Location = new System.Drawing.Point(96, 79);
            this.demandLabel.Name = "demandLabel";
            this.demandLabel.Size = new System.Drawing.Size(50, 13);
            this.demandLabel.TabIndex = 61;
            this.demandLabel.Text = "Demand:";
            this.toolTip1.SetToolTip(this.demandLabel, "Filter hops below this level of demand");
            // 
            // showJumpsCheckBox
            // 
            this.showJumpsCheckBox.AutoSize = true;
            this.showJumpsCheckBox.Location = new System.Drawing.Point(10, 99);
            this.showJumpsCheckBox.Name = "showJumpsCheckBox";
            this.showJumpsCheckBox.Size = new System.Drawing.Size(56, 17);
            this.showJumpsCheckBox.TabIndex = 62;
            this.showJumpsCheckBox.TabStop = false;
            this.showJumpsCheckBox.Text = "Jumps";
            this.toolTip1.SetToolTip(this.showJumpsCheckBox, "Show jumps between hops during a multi-hop route");
            this.showJumpsCheckBox.UseVisualStyleBackColor = true;
            // 
            // miscSettingsButton
            // 
            this.miscSettingsButton.Image = global::TDHelper.Properties.Resources.Gear;
            this.miscSettingsButton.Location = new System.Drawing.Point(9, 32);
            this.miscSettingsButton.Name = "miscSettingsButton";
            this.miscSettingsButton.Size = new System.Drawing.Size(16, 16);
            this.miscSettingsButton.TabIndex = 63;
            this.miscSettingsButton.TabStop = false;
            this.toolTip1.SetToolTip(this.miscSettingsButton, "Modify additional configuration settings");
            this.miscSettingsButton.UseVisualStyleBackColor = true;
            this.miscSettingsButton.Click += new System.EventHandler(this.miscSettingsButton_Click);
            // 
            // verbosityComboBox
            // 
            this.verbosityComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.verbosityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.verbosityComboBox.Items.AddRange(new object[] {
            "",
            "-v",
            "-vv",
            "-vvv"});
            this.verbosityComboBox.Location = new System.Drawing.Point(491, 12);
            this.verbosityComboBox.Name = "verbosityComboBox";
            this.verbosityComboBox.Size = new System.Drawing.Size(46, 21);
            this.verbosityComboBox.TabIndex = 9;
            this.verbosityComboBox.TabStop = false;
            // 
            // backgroundWorker3
            // 
            this.backgroundWorker3.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker3_DoWork);
            this.backgroundWorker3.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker3_RunWorkerCompleted);
            // 
            // backgroundWorker4
            // 
            this.backgroundWorker4.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker4_DoWork);
            this.backgroundWorker4.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker4_RunWorkerCompleted);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.miscSettingsButton);
            this.panel1.Controls.Add(this.showJumpsCheckBox);
            this.panel1.Controls.Add(this.shortenCheckBox);
            this.panel1.Controls.Add(this.updateNotifyLabel);
            this.panel1.Controls.Add(this.updateNotifyIcon);
            this.panel1.Controls.Add(this.destSysLabel);
            this.panel1.Controls.Add(this.bmktCheckBox);
            this.panel1.Controls.Add(this.oneStopCheckBox);
            this.panel1.Controls.Add(this.endJumpsLabel);
            this.panel1.Controls.Add(this.startJumpsLabel);
            this.panel1.Controls.Add(this.directCheckBox);
            this.panel1.Controls.Add(this.belowPriceBox);
            this.panel1.Controls.Add(this.commodityLabel);
            this.panel1.Controls.Add(this.abovePriceBox);
            this.panel1.Controls.Add(this.commodityComboBox);
            this.panel1.Controls.Add(this.loopCheckBox);
            this.panel1.Controls.Add(this.endJumpsBox);
            this.panel1.Controls.Add(this.abovePriceLabel);
            this.panel1.Controls.Add(this.startJumpsBox);
            this.panel1.Controls.Add(this.towardsCheckBox);
            this.panel1.Controls.Add(this.belowPriceLabel);
            this.panel1.Controls.Add(this.destSystemComboBox);
            this.panel1.Controls.Add(this.uniqueCheckBox);
            this.panel1.Location = new System.Drawing.Point(12, 35);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(312, 159);
            this.panel1.TabIndex = 1;
            // 
            // updateNotifyLabel
            // 
            this.updateNotifyLabel.AutoSize = true;
            this.updateNotifyLabel.Location = new System.Drawing.Point(31, 142);
            this.updateNotifyLabel.Name = "updateNotifyLabel";
            this.updateNotifyLabel.Size = new System.Drawing.Size(263, 13);
            this.updateNotifyLabel.TabIndex = 60;
            this.updateNotifyLabel.Text = "An update to TDHelper is available! Restart to update.";
            this.updateNotifyLabel.Visible = false;
            // 
            // localFilterParentPanel
            // 
            this.localFilterParentPanel.Controls.Add(this.localFilterGroupBox);
            this.localFilterParentPanel.Location = new System.Drawing.Point(12, 35);
            this.localFilterParentPanel.Name = "localFilterParentPanel";
            this.localFilterParentPanel.Size = new System.Drawing.Size(312, 159);
            this.localFilterParentPanel.TabIndex = 60;
            this.localFilterParentPanel.Visible = false;
            // 
            // localFilterGroupBox
            // 
            this.localFilterGroupBox.Controls.Add(this.stationsFilterCheckBox);
            this.localFilterGroupBox.Controls.Add(this.resetFilterButton);
            this.localFilterGroupBox.Controls.Add(this.outfitFilterCheckBox);
            this.localFilterGroupBox.Controls.Add(this.rearmFilterCheckBox);
            this.localFilterGroupBox.Controls.Add(this.itemsFilterCheckBox);
            this.localFilterGroupBox.Controls.Add(this.repairFilterCheckBox);
            this.localFilterGroupBox.Controls.Add(this.bmktFilterCheckBox);
            this.localFilterGroupBox.Controls.Add(this.refuelFilterCheckBox);
            this.localFilterGroupBox.Controls.Add(this.shipyardFilterCheckBox);
            this.localFilterGroupBox.Location = new System.Drawing.Point(75, 8);
            this.localFilterGroupBox.Name = "localFilterGroupBox";
            this.localFilterGroupBox.Size = new System.Drawing.Size(163, 121);
            this.localFilterGroupBox.TabIndex = 59;
            this.localFilterGroupBox.TabStop = false;
            this.localFilterGroupBox.Text = "Filters";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.outputPage);
            this.tabControl1.Controls.Add(this.savedPage1);
            this.tabControl1.Controls.Add(this.savedPage2);
            this.tabControl1.Controls.Add(this.savedPage3);
            this.tabControl1.Controls.Add(this.notesPage);
            this.tabControl1.Controls.Add(this.logPage);
            this.tabControl1.Location = new System.Drawing.Point(12, 209);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(801, 498);
            this.tabControl1.TabIndex = 23;
            this.tabControl1.TabStop = false;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // outputPage
            // 
            this.outputPage.Controls.Add(this.td_outputBox);
            this.outputPage.Location = new System.Drawing.Point(4, 22);
            this.outputPage.Name = "outputPage";
            this.outputPage.Padding = new System.Windows.Forms.Padding(3);
            this.outputPage.Size = new System.Drawing.Size(793, 472);
            this.outputPage.TabIndex = 0;
            this.outputPage.Text = "Output";
            this.outputPage.UseVisualStyleBackColor = true;
            // 
            // td_outputBox
            // 
            this.td_outputBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.td_outputBox.ContextMenuStrip = this.contextMenuStrip1;
            this.td_outputBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.td_outputBox.Location = new System.Drawing.Point(3, 3);
            this.td_outputBox.Name = "td_outputBox";
            this.td_outputBox.ReadOnly = true;
            this.td_outputBox.Size = new System.Drawing.Size(787, 466);
            this.td_outputBox.TabIndex = 0;
            this.td_outputBox.TabStop = false;
            this.td_outputBox.Text = "";
            this.td_outputBox.TextChanged += new System.EventHandler(this.td_outputBox_TextChanged);
            // 
            // savedPage1
            // 
            this.savedPage1.Controls.Add(this.savedTextBox1);
            this.savedPage1.Location = new System.Drawing.Point(4, 22);
            this.savedPage1.Name = "savedPage1";
            this.savedPage1.Padding = new System.Windows.Forms.Padding(3);
            this.savedPage1.Size = new System.Drawing.Size(793, 472);
            this.savedPage1.TabIndex = 1;
            this.savedPage1.Text = "Saved #1";
            this.savedPage1.UseVisualStyleBackColor = true;
            // 
            // savedTextBox1
            // 
            this.savedTextBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.savedTextBox1.ContextMenuStrip = this.contextMenuStrip1;
            this.savedTextBox1.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedTextBox1.Location = new System.Drawing.Point(3, 3);
            this.savedTextBox1.Name = "savedTextBox1";
            this.savedTextBox1.ReadOnly = true;
            this.savedTextBox1.Size = new System.Drawing.Size(787, 466);
            this.savedTextBox1.TabIndex = 2;
            this.savedTextBox1.TabStop = false;
            this.savedTextBox1.Text = "";
            // 
            // savedPage2
            // 
            this.savedPage2.Controls.Add(this.savedTextBox2);
            this.savedPage2.Location = new System.Drawing.Point(4, 22);
            this.savedPage2.Name = "savedPage2";
            this.savedPage2.Size = new System.Drawing.Size(793, 472);
            this.savedPage2.TabIndex = 2;
            this.savedPage2.Text = "Saved #2";
            this.savedPage2.UseVisualStyleBackColor = true;
            // 
            // savedTextBox2
            // 
            this.savedTextBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.savedTextBox2.ContextMenuStrip = this.contextMenuStrip1;
            this.savedTextBox2.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedTextBox2.Location = new System.Drawing.Point(3, 3);
            this.savedTextBox2.Name = "savedTextBox2";
            this.savedTextBox2.ReadOnly = true;
            this.savedTextBox2.Size = new System.Drawing.Size(787, 466);
            this.savedTextBox2.TabIndex = 2;
            this.savedTextBox2.TabStop = false;
            this.savedTextBox2.Text = "";
            // 
            // savedPage3
            // 
            this.savedPage3.Controls.Add(this.savedTextBox3);
            this.savedPage3.Location = new System.Drawing.Point(4, 22);
            this.savedPage3.Name = "savedPage3";
            this.savedPage3.Size = new System.Drawing.Size(793, 472);
            this.savedPage3.TabIndex = 3;
            this.savedPage3.Text = "Saved #3";
            this.savedPage3.UseVisualStyleBackColor = true;
            // 
            // savedTextBox3
            // 
            this.savedTextBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.savedTextBox3.ContextMenuStrip = this.contextMenuStrip1;
            this.savedTextBox3.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.savedTextBox3.Location = new System.Drawing.Point(3, 3);
            this.savedTextBox3.Name = "savedTextBox3";
            this.savedTextBox3.ReadOnly = true;
            this.savedTextBox3.Size = new System.Drawing.Size(787, 466);
            this.savedTextBox3.TabIndex = 2;
            this.savedTextBox3.TabStop = false;
            this.savedTextBox3.Text = "";
            // 
            // notesPage
            // 
            this.notesPage.Controls.Add(this.notesTextBox);
            this.notesPage.Location = new System.Drawing.Point(4, 22);
            this.notesPage.Name = "notesPage";
            this.notesPage.Padding = new System.Windows.Forms.Padding(3);
            this.notesPage.Size = new System.Drawing.Size(793, 472);
            this.notesPage.TabIndex = 4;
            this.notesPage.Text = "Notes";
            this.notesPage.UseVisualStyleBackColor = true;
            // 
            // notesTextBox
            // 
            this.notesTextBox.AcceptsTab = true;
            this.notesTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.notesTextBox.ContextMenuStrip = this.contextMenuStrip1;
            this.notesTextBox.Font = new System.Drawing.Font("Consolas", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.notesTextBox.Location = new System.Drawing.Point(3, 3);
            this.notesTextBox.Name = "notesTextBox";
            this.notesTextBox.Size = new System.Drawing.Size(787, 466);
            this.notesTextBox.TabIndex = 3;
            this.notesTextBox.TabStop = false;
            this.notesTextBox.Text = "";
            // 
            // logPage
            // 
            this.logPage.Controls.Add(this.pilotsLogDataGrid);
            this.logPage.Location = new System.Drawing.Point(4, 22);
            this.logPage.Name = "logPage";
            this.logPage.Padding = new System.Windows.Forms.Padding(3);
            this.logPage.Size = new System.Drawing.Size(793, 472);
            this.logPage.TabIndex = 5;
            this.logPage.Text = "Pilot\'s Log";
            this.logPage.UseVisualStyleBackColor = true;
            // 
            // pilotsLogDataGrid
            // 
            this.pilotsLogDataGrid.AllowUserToAddRows = false;
            this.pilotsLogDataGrid.AllowUserToResizeRows = false;
            this.pilotsLogDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pilotsLogDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.pilotsLogDataGrid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.pilotsLogDataGrid.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pilotsLogDataGrid.ContextMenuStrip = this.contextMenuStrip2;
            this.pilotsLogDataGrid.Location = new System.Drawing.Point(3, 3);
            this.pilotsLogDataGrid.Name = "pilotsLogDataGrid";
            this.pilotsLogDataGrid.Size = new System.Drawing.Size(787, 466);
            this.pilotsLogDataGrid.TabIndex = 0;
            this.pilotsLogDataGrid.VirtualMode = true;
            this.pilotsLogDataGrid.CellContextMenuStripNeeded += new System.Windows.Forms.DataGridViewCellContextMenuStripNeededEventHandler(this.pilotsLogDataGrid_CellContextMenuStripNeeded);
            this.pilotsLogDataGrid.CellValueNeeded += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.pilotsLogDataGrid_CellValueNeeded);
            this.pilotsLogDataGrid.CellValuePushed += new System.Windows.Forms.DataGridViewCellValueEventHandler(this.pilotsLogDataGrid_CellValuePushed);
            this.pilotsLogDataGrid.UserDeletingRow += new System.Windows.Forms.DataGridViewRowCancelEventHandler(this.pilotsLogDataGrid_UserDeletingRow);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.insertAtGridRow,
            this.removeAtGridRow,
            this.toolStripSeparator4,
            this.forceRefreshGridView,
            this.forceResortMenuItem,
            this.toolStripSeparator5,
            this.copySystemToSrc,
            this.copySystemToDest,
            this.copySystemToEDSC});
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(189, 192);
            // 
            // insertAtGridRow
            // 
            this.insertAtGridRow.Name = "insertAtGridRow";
            this.insertAtGridRow.Size = new System.Drawing.Size(188, 22);
            this.insertAtGridRow.Text = "Insert Row";
            this.insertAtGridRow.Click += new System.EventHandler(this.insertAtGridRow_Click);
            // 
            // removeAtGridRow
            // 
            this.removeAtGridRow.Name = "removeAtGridRow";
            this.removeAtGridRow.Size = new System.Drawing.Size(188, 22);
            this.removeAtGridRow.Text = "Remove Row";
            this.removeAtGridRow.Click += new System.EventHandler(this.removeAtGridRow_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(185, 6);
            // 
            // forceRefreshGridView
            // 
            this.forceRefreshGridView.Name = "forceRefreshGridView";
            this.forceRefreshGridView.Size = new System.Drawing.Size(188, 22);
            this.forceRefreshGridView.Text = "Force Refresh";
            this.forceRefreshGridView.Click += new System.EventHandler(this.forceRefreshGridView_Click);
            // 
            // forceResortMenuItem
            // 
            this.forceResortMenuItem.Name = "forceResortMenuItem";
            this.forceResortMenuItem.Size = new System.Drawing.Size(188, 22);
            this.forceResortMenuItem.Text = "Force Re-sort";
            this.forceResortMenuItem.Click += new System.EventHandler(this.forceResortMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(185, 6);
            // 
            // copySystemToSrc
            // 
            this.copySystemToSrc.Name = "copySystemToSrc";
            this.copySystemToSrc.Size = new System.Drawing.Size(188, 22);
            this.copySystemToSrc.Text = "Copy System to Src";
            this.copySystemToSrc.Click += new System.EventHandler(this.copySystemToSrc_Click);
            // 
            // copySystemToDest
            // 
            this.copySystemToDest.Name = "copySystemToDest";
            this.copySystemToDest.Size = new System.Drawing.Size(188, 22);
            this.copySystemToDest.Text = "Copy System to Dest";
            this.copySystemToDest.Click += new System.EventHandler(this.copySystemToDest_Click);
            // 
            // jumpsLabel
            // 
            this.jumpsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.jumpsLabel.AutoSize = true;
            this.jumpsLabel.Location = new System.Drawing.Point(254, 10);
            this.jumpsLabel.Name = "jumpsLabel";
            this.jumpsLabel.Size = new System.Drawing.Size(40, 13);
            this.jumpsLabel.TabIndex = 31;
            this.jumpsLabel.Text = "Jumps:";
            // 
            // maxLSLabel
            // 
            this.maxLSLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.maxLSLabel.AutoSize = true;
            this.maxLSLabel.Location = new System.Drawing.Point(239, 125);
            this.maxLSLabel.Name = "maxLSLabel";
            this.maxLSLabel.Size = new System.Drawing.Size(46, 13);
            this.maxLSLabel.TabIndex = 10;
            this.maxLSLabel.Text = "Max LS:";
            // 
            // pruneHopsLabel
            // 
            this.pruneHopsLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pruneHopsLabel.AutoSize = true;
            this.pruneHopsLabel.Location = new System.Drawing.Point(10, 149);
            this.pruneHopsLabel.Name = "pruneHopsLabel";
            this.pruneHopsLabel.Size = new System.Drawing.Size(66, 13);
            this.pruneHopsLabel.TabIndex = 15;
            this.pruneHopsLabel.Text = "Prune Hops:";
            // 
            // pruneScoreLabel
            // 
            this.pruneScoreLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pruneScoreLabel.AutoSize = true;
            this.pruneScoreLabel.Location = new System.Drawing.Point(7, 172);
            this.pruneScoreLabel.Name = "pruneScoreLabel";
            this.pruneScoreLabel.Size = new System.Drawing.Size(69, 13);
            this.pruneScoreLabel.TabIndex = 49;
            this.pruneScoreLabel.Text = "Prune Score:";
            // 
            // lsPenaltyLabel
            // 
            this.lsPenaltyLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lsPenaltyLabel.AutoSize = true;
            this.lsPenaltyLabel.Location = new System.Drawing.Point(101, 125);
            this.lsPenaltyLabel.Name = "lsPenaltyLabel";
            this.lsPenaltyLabel.Size = new System.Drawing.Size(61, 13);
            this.lsPenaltyLabel.TabIndex = 8;
            this.lsPenaltyLabel.Text = "LS Penalty:";
            // 
            // insuranceLabel
            // 
            this.insuranceLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.insuranceLabel.AutoSize = true;
            this.insuranceLabel.Location = new System.Drawing.Point(183, 56);
            this.insuranceLabel.Name = "insuranceLabel";
            this.insuranceLabel.Size = new System.Drawing.Size(57, 13);
            this.insuranceLabel.TabIndex = 51;
            this.insuranceLabel.Text = "Insurance:";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.shipsSoldBox);
            this.panel2.Controls.Add(this.oldRoutesCheckBox);
            this.panel2.Controls.Add(this.minAgeLabel);
            this.panel2.Controls.Add(this.minAgeUpDown);
            this.panel2.Controls.Add(this.resetStationButton);
            this.panel2.Controls.Add(this.panelLocalOverrideChild);
            this.panel2.Controls.Add(this.csvExportComboBox);
            this.panel2.Controls.Add(this.csvExportCheckBox);
            this.panel2.Controls.Add(this.shipsSoldLabel);
            this.panel2.Controls.Add(this.stn_padSizeBox);
            this.panel2.Controls.Add(this.lsFromStarBox);
            this.panel2.Controls.Add(this.maxPadSizeLabel);
            this.panel2.Controls.Add(this.lsFromStarLabel);
            this.panel2.Location = new System.Drawing.Point(12, 35);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(312, 159);
            this.panel2.TabIndex = 3;
            // 
            // panelLocalOverrideChild
            // 
            this.panelLocalOverrideChild.Controls.Add(this.confirmLabel);
            this.panelLocalOverrideChild.Controls.Add(this.confirmBox);
            this.panelLocalOverrideChild.Controls.Add(this.outfitCheckBox);
            this.panelLocalOverrideChild.Controls.Add(this.rearmCheckBox);
            this.panelLocalOverrideChild.Controls.Add(this.repairCheckBox);
            this.panelLocalOverrideChild.Controls.Add(this.refuelCheckBox);
            this.panelLocalOverrideChild.Controls.Add(this.shipyardCheckBox);
            this.panelLocalOverrideChild.Controls.Add(this.blackMarketCheckBox);
            this.panelLocalOverrideChild.Controls.Add(this.marketCheckBox);
            this.panelLocalOverrideChild.Location = new System.Drawing.Point(28, 33);
            this.panelLocalOverrideChild.Name = "panelLocalOverrideChild";
            this.panelLocalOverrideChild.Size = new System.Drawing.Size(185, 100);
            this.panelLocalOverrideChild.TabIndex = 59;
            // 
            // confirmLabel
            // 
            this.confirmLabel.AutoSize = true;
            this.confirmLabel.Location = new System.Drawing.Point(90, 71);
            this.confirmLabel.Name = "confirmLabel";
            this.confirmLabel.Size = new System.Drawing.Size(45, 13);
            this.confirmLabel.TabIndex = 60;
            this.confirmLabel.Text = "Confirm:";
            // 
            // runOptionsPanel
            // 
            this.runOptionsPanel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.runOptionsPanel.Controls.Add(this.demandBox);
            this.runOptionsPanel.Controls.Add(this.demandLabel);
            this.runOptionsPanel.Controls.Add(this.loopIntLabel);
            this.runOptionsPanel.Controls.Add(this.loopIntBox);
            this.runOptionsPanel.Controls.Add(this.stockBox);
            this.runOptionsPanel.Controls.Add(this.stockLabel);
            this.runOptionsPanel.Controls.Add(this.maxGPTLabel);
            this.runOptionsPanel.Controls.Add(this.maxGPTBox);
            this.runOptionsPanel.Controls.Add(this.marginLabel);
            this.runOptionsPanel.Controls.Add(this.marginBox);
            this.runOptionsPanel.Controls.Add(this.hopsLabel);
            this.runOptionsPanel.Controls.Add(this.stationDropDown);
            this.runOptionsPanel.Controls.Add(this.avoidLabel);
            this.runOptionsPanel.Controls.Add(this.jumpsLabel);
            this.runOptionsPanel.Controls.Add(this.avoidBox);
            this.runOptionsPanel.Controls.Add(this.gptLabel);
            this.runOptionsPanel.Controls.Add(this.viaLabel);
            this.runOptionsPanel.Controls.Add(this.ladenLYLabel);
            this.runOptionsPanel.Controls.Add(this.viaBox);
            this.runOptionsPanel.Controls.Add(this.maxLSLabel);
            this.runOptionsPanel.Controls.Add(this.pruneHopsLabel);
            this.runOptionsPanel.Controls.Add(this.pruneScoreLabel);
            this.runOptionsPanel.Controls.Add(this.limitBox);
            this.runOptionsPanel.Controls.Add(this.lsPenaltyLabel);
            this.runOptionsPanel.Controls.Add(this.insuranceLabel);
            this.runOptionsPanel.Controls.Add(this.pruneHopsBox);
            this.runOptionsPanel.Controls.Add(this.unladenLYLabel);
            this.runOptionsPanel.Controls.Add(this.limitLabel);
            this.runOptionsPanel.Controls.Add(this.pruneScoreBox);
            this.runOptionsPanel.Controls.Add(this.creditsLabel);
            this.runOptionsPanel.Controls.Add(this.capacityLabel);
            this.runOptionsPanel.Controls.Add(this.lsPenaltyBox);
            this.runOptionsPanel.Controls.Add(this.ladenLYBox);
            this.runOptionsPanel.Controls.Add(this.unladenLYBox);
            this.runOptionsPanel.Controls.Add(this.maxLSDistanceBox);
            this.runOptionsPanel.Controls.Add(this.creditsBox);
            this.runOptionsPanel.Controls.Add(this.hopsBox);
            this.runOptionsPanel.Controls.Add(this.gptBox);
            this.runOptionsPanel.Controls.Add(this.jumpsBox);
            this.runOptionsPanel.Controls.Add(this.capacityBox);
            this.runOptionsPanel.Controls.Add(this.insuranceBox);
            this.runOptionsPanel.Location = new System.Drawing.Point(340, 35);
            this.runOptionsPanel.Name = "runOptionsPanel";
            this.runOptionsPanel.Size = new System.Drawing.Size(473, 193);
            this.runOptionsPanel.TabIndex = 56;
            // 
            // backgroundWorker5
            // 
            this.backgroundWorker5.WorkerReportsProgress = true;
            this.backgroundWorker5.WorkerSupportsCancellation = true;
            this.backgroundWorker5.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker5_DoWork);
            this.backgroundWorker5.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker5_RunWorkerCompleted);
            // 
            // backgroundWorker6
            // 
            this.backgroundWorker6.WorkerReportsProgress = true;
            this.backgroundWorker6.WorkerSupportsCancellation = true;
            this.backgroundWorker6.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker6_DoWork);
            this.backgroundWorker6.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker6_RunWorkerCompleted);
            // 
            // edscPanel
            // 
            this.edscPanel.Controls.Add(this.crFilterUpDown);
            this.edscPanel.Controls.Add(this.crFilterLabel);
            this.edscPanel.Controls.Add(this.edscLYBox5);
            this.edscPanel.Controls.Add(this.edscLYBox4);
            this.edscPanel.Controls.Add(this.edscLYBox3);
            this.edscPanel.Controls.Add(this.edscLYBox2);
            this.edscPanel.Controls.Add(this.edscLYBox1);
            this.edscPanel.Controls.Add(this.cmdrNameTextBox);
            this.edscPanel.Controls.Add(this.cmdrNameLabel);
            this.edscPanel.Controls.Add(this.edscLYLabel5);
            this.edscPanel.Controls.Add(this.edscLYLabel4);
            this.edscPanel.Controls.Add(this.edscLYLabel3);
            this.edscPanel.Controls.Add(this.edscLYLabel2);
            this.edscPanel.Controls.Add(this.refSysTextBox5);
            this.edscPanel.Controls.Add(this.refSysLabel5);
            this.edscPanel.Controls.Add(this.refSysTextBox4);
            this.edscPanel.Controls.Add(this.refSysLabel4);
            this.edscPanel.Controls.Add(this.refSysTextBox3);
            this.edscPanel.Controls.Add(this.refSysLabel3);
            this.edscPanel.Controls.Add(this.refSysTextBox2);
            this.edscPanel.Controls.Add(this.refSysLabel2);
            this.edscPanel.Controls.Add(this.edscLYLabel1);
            this.edscPanel.Controls.Add(this.refSysTextBox1);
            this.edscPanel.Controls.Add(this.refSysLabel1);
            this.edscPanel.Location = new System.Drawing.Point(12, 35);
            this.edscPanel.Name = "edscPanel";
            this.edscPanel.Size = new System.Drawing.Size(312, 159);
            this.edscPanel.TabIndex = 65;
            this.edscPanel.Visible = false;
            // 
            // trackerLinkLabel
            // 
            this.trackerLinkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.trackerLinkLabel.AutoSize = true;
            this.trackerLinkLabel.Location = new System.Drawing.Point(9, 737);
            this.trackerLinkLabel.Name = "trackerLinkLabel";
            this.trackerLinkLabel.Size = new System.Drawing.Size(99, 13);
            this.trackerLinkLabel.TabIndex = 66;
            this.trackerLinkLabel.TabStop = true;
            this.trackerLinkLabel.Text = "Report bugs/issues";
            this.trackerLinkLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.trackerLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.trackerLinkLabel_LinkClicked);
            // 
            // faqLinkLabel
            // 
            this.faqLinkLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.faqLinkLabel.AutoSize = true;
            this.faqLinkLabel.Location = new System.Drawing.Point(758, 739);
            this.faqLinkLabel.Name = "faqLinkLabel";
            this.faqLinkLabel.Size = new System.Drawing.Size(55, 13);
            this.faqLinkLabel.TabIndex = 67;
            this.faqLinkLabel.TabStop = true;
            this.faqLinkLabel.Text = "Help/FAQ";
            this.faqLinkLabel.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.faqLinkLabel_LinkClicked);
            // 
            // copySystemToEDSC
            // 
            this.copySystemToEDSC.Name = "copySystemToEDSC";
            this.copySystemToEDSC.Size = new System.Drawing.Size(188, 22);
            this.copySystemToEDSC.Text = "Copy System to EDSC";
            this.copySystemToEDSC.Click += new System.EventHandler(this.copySystemToEDSC_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(826, 759);
            this.Controls.Add(this.faqLinkLabel);
            this.Controls.Add(this.trackerLinkLabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.edscPanel);
            this.Controls.Add(this.localFilterParentPanel);
            this.Controls.Add(this.testSystemsCheckBox);
            this.Controls.Add(this.localNavCheckBox);
            this.Controls.Add(this.methodDropDown);
            this.Controls.Add(this.correctCheckBox);
            this.Controls.Add(this.altConfigBox);
            this.Controls.Add(this.miniModeButton);
            this.Controls.Add(this.swapButton);
            this.Controls.Add(this.skipImportCheckBox);
            this.Controls.Add(this.forceUpdateBox);
            this.Controls.Add(this.runOptionsPanel);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.uploadButton);
            this.Controls.Add(this.importButton);
            this.Controls.Add(this.editCommodButton);
            this.Controls.Add(this.ageBox);
            this.Controls.Add(this.verboseLabel);
            this.Controls.Add(this.verbosityComboBox);
            this.Controls.Add(this.ageLabel);
            this.Controls.Add(this.loadSettingsButton);
            this.Controls.Add(this.padSizeBox);
            this.Controls.Add(this.padSizeLabel);
            this.Controls.Add(this.updateButton);
            this.Controls.Add(this.saveSettingsButton);
            this.Controls.Add(this.srcSysLabel);
            this.Controls.Add(this.srcSystemComboBox);
            this.Controls.Add(this.getSystemButton);
            this.Controls.Add(this.runButton);
            this.Controls.Add(this.stopWatchLabel);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(842, 370);
            this.Name = "Form1";
            this.Text = "Trade Dangerous Helper";
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.LocationChanged += new System.EventHandler(this.Form1_LocationChanged);
            this.contextMenuStrip1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.belowPriceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.abovePriceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.endJumpsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.startJumpsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.limitBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pruneHopsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pruneScoreBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsPenaltyBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxLSDistanceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gptBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.insuranceBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.capacityBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.jumpsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.hopsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.creditsBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.unladenLYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ladenLYBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ageBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lsFromStarBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.marginBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.maxGPTBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.updateNotifyIcon)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.stockBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.minAgeUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.loopIntBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edscLYBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.crFilterUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.demandBox)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.localFilterParentPanel.ResumeLayout(false);
            this.localFilterGroupBox.ResumeLayout(false);
            this.localFilterGroupBox.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.outputPage.ResumeLayout(false);
            this.savedPage1.ResumeLayout(false);
            this.savedPage2.ResumeLayout(false);
            this.savedPage3.ResumeLayout(false);
            this.notesPage.ResumeLayout(false);
            this.logPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pilotsLogDataGrid)).EndInit();
            this.contextMenuStrip2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panelLocalOverrideChild.ResumeLayout(false);
            this.panelLocalOverrideChild.PerformLayout();
            this.runOptionsPanel.ResumeLayout(false);
            this.runOptionsPanel.PerformLayout();
            this.edscPanel.ResumeLayout(false);
            this.edscPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.Label destSysLabel;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox destSystemComboBox;
        private System.Windows.Forms.Label stopWatchLabel;
        private System.ComponentModel.BackgroundWorker backgroundWorker2;
        private System.Windows.Forms.Label unladenLYLabel;
        private System.Windows.Forms.Label ladenLYLabel;
        private System.Windows.Forms.Label capacityLabel;
        private System.Windows.Forms.Label creditsLabel;
        private System.Windows.Forms.CheckBox towardsCheckBox;
        private System.Windows.Forms.CheckBox loopCheckBox;
        private System.Windows.Forms.Button saveSettingsButton;
        private System.Windows.Forms.ComboBox commodityComboBox;
        private System.Windows.Forms.Label commodityLabel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label hopsLabel;
        private System.Windows.Forms.Button updateButton;
        private System.Windows.Forms.Button loadSettingsButton;
        private System.Windows.Forms.CheckBox localNavCheckBox;
        private System.Windows.Forms.CheckBox bmktCheckBox;
        private System.Windows.Forms.Label avoidLabel;
        private System.Windows.Forms.TextBox avoidBox;
        private System.Windows.Forms.Label startJumpsLabel;
        private System.Windows.Forms.Label endJumpsLabel;
        private System.Windows.Forms.TextBox viaBox;
        private System.Windows.Forms.Label viaLabel;
        private System.Windows.Forms.ComboBox methodDropDown;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem cutMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pasteMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem selectMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem pushNotesMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notesClearMenuItem;
        private System.Windows.Forms.Label belowPriceLabel;
        private System.Windows.Forms.Label abovePriceLabel;
        private System.Windows.Forms.NumericUpDown unladenLYBox;
        private System.Windows.Forms.NumericUpDown ladenLYBox;
        private System.Windows.Forms.NumericUpDown capacityBox;
        private System.Windows.Forms.NumericUpDown jumpsBox;
        private System.Windows.Forms.NumericUpDown hopsBox;
        private System.Windows.Forms.NumericUpDown creditsBox;
        private System.Windows.Forms.NumericUpDown insuranceBox;
        private System.Windows.Forms.CheckBox uniqueCheckBox;
        private System.Windows.Forms.Button getSystemButton;
        private System.Windows.Forms.ComboBox srcSystemComboBox;
        private System.Windows.Forms.Label srcSysLabel;
        private System.Windows.Forms.Label padSizeLabel;
        private System.Windows.Forms.TextBox padSizeBox;
        private System.Windows.Forms.Label ageLabel;
        private System.Windows.Forms.ComboBox verbosityComboBox;
        private System.Windows.Forms.Label verboseLabel;
        private System.Windows.Forms.NumericUpDown maxLSDistanceBox;
        private System.Windows.Forms.NumericUpDown gptBox;
        private System.Windows.Forms.NumericUpDown limitBox;
        private System.Windows.Forms.NumericUpDown pruneHopsBox;
        private System.Windows.Forms.NumericUpDown pruneScoreBox;
        private System.Windows.Forms.NumericUpDown lsPenaltyBox;
        private System.Windows.Forms.NumericUpDown ageBox;
        private System.Windows.Forms.NumericUpDown belowPriceBox;
        private System.Windows.Forms.NumericUpDown abovePriceBox;
        private System.Windows.Forms.NumericUpDown endJumpsBox;
        private System.Windows.Forms.NumericUpDown startJumpsBox;
        private System.Windows.Forms.ToolStripMenuItem deleteMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker3;
        private System.ComponentModel.BackgroundWorker backgroundWorker4;
        private System.Windows.Forms.CheckBox directCheckBox;
        private System.Windows.Forms.CheckBox oneStopCheckBox;
        private System.Windows.Forms.Button editCommodButton;
        private System.Windows.Forms.ComboBox stationDropDown;
        private System.Windows.Forms.Button importButton;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem savePage1MenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePage2MenuItem;
        private System.Windows.Forms.ToolStripMenuItem savePage3MenuItem;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage outputPage;
        private System.Windows.Forms.RichTextBox td_outputBox;
        private System.Windows.Forms.TabPage savedPage1;
        private System.Windows.Forms.RichTextBox savedTextBox1;
        private System.Windows.Forms.TabPage savedPage2;
        private System.Windows.Forms.RichTextBox savedTextBox2;
        private System.Windows.Forms.TabPage savedPage3;
        private System.Windows.Forms.RichTextBox savedTextBox3;
        private System.Windows.Forms.TabPage notesPage;
        private System.Windows.Forms.RichTextBox notesTextBox;
        private System.Windows.Forms.Label jumpsLabel;
        private System.Windows.Forms.Label maxLSLabel;
        private System.Windows.Forms.Label pruneHopsLabel;
        private System.Windows.Forms.Label pruneScoreLabel;
        private System.Windows.Forms.Label lsPenaltyLabel;
        private System.Windows.Forms.Label insuranceLabel;
        private System.Windows.Forms.Label limitLabel;
        private System.Windows.Forms.Label gptLabel;
        private System.Windows.Forms.ToolStripMenuItem clearSaved1MenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearSaved2MenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearSaved3MenuItem;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button uploadButton;
        private System.Windows.Forms.CheckBox shipyardCheckBox;
        private System.Windows.Forms.CheckBox marketCheckBox;
        private System.Windows.Forms.CheckBox blackMarketCheckBox;
        private System.Windows.Forms.NumericUpDown lsFromStarBox;
        private System.Windows.Forms.Label maxPadSizeLabel;
        private System.Windows.Forms.Label lsFromStarLabel;
        private System.Windows.Forms.TextBox stn_padSizeBox;
        private System.Windows.Forms.Label shipsSoldLabel;
        private System.Windows.Forms.Panel runOptionsPanel;
        private System.Windows.Forms.CheckBox csvExportCheckBox;
        private System.Windows.Forms.ComboBox csvExportComboBox;
        private System.Windows.Forms.Label marginLabel;
        private System.Windows.Forms.NumericUpDown marginBox;
        private System.Windows.Forms.Label maxGPTLabel;
        private System.Windows.Forms.NumericUpDown maxGPTBox;
        private System.Windows.Forms.CheckBox rearmCheckBox;
        private System.Windows.Forms.CheckBox repairCheckBox;
        private System.Windows.Forms.CheckBox refuelCheckBox;
        private System.Windows.Forms.CheckBox outfitCheckBox;
        private System.Windows.Forms.Panel panelLocalOverrideChild;
        private System.Windows.Forms.Panel localFilterParentPanel;
        private System.Windows.Forms.GroupBox localFilterGroupBox;
        private System.Windows.Forms.CheckBox outfitFilterCheckBox;
        private System.Windows.Forms.CheckBox rearmFilterCheckBox;
        private System.Windows.Forms.CheckBox itemsFilterCheckBox;
        private System.Windows.Forms.CheckBox repairFilterCheckBox;
        private System.Windows.Forms.CheckBox bmktFilterCheckBox;
        private System.Windows.Forms.CheckBox refuelFilterCheckBox;
        private System.Windows.Forms.CheckBox shipyardFilterCheckBox;
        private System.Windows.Forms.ComboBox forceUpdateBox;
        private System.Windows.Forms.CheckBox skipImportCheckBox;
        private System.Windows.Forms.Button resetFilterButton;
        private System.Windows.Forms.Button resetStationButton;
        private System.Windows.Forms.Label confirmLabel;
        private System.Windows.Forms.TextBox confirmBox;
        private System.Windows.Forms.Button swapButton;
        private System.Windows.Forms.Button miniModeButton;
        private System.Windows.Forms.ComboBox altConfigBox;
        private System.Windows.Forms.CheckBox correctCheckBox;
        private System.Windows.Forms.Label updateNotifyLabel;
        private System.Windows.Forms.PictureBox updateNotifyIcon;
        private System.ComponentModel.BackgroundWorker backgroundWorker5;
        private System.Windows.Forms.NumericUpDown stockBox;
        private System.Windows.Forms.Label stockLabel;
        private System.Windows.Forms.CheckBox testSystemsCheckBox;
        private System.ComponentModel.BackgroundWorker backgroundWorker6;
        private System.Windows.Forms.CheckBox stationsFilterCheckBox;
        private System.Windows.Forms.Label minAgeLabel;
        private System.Windows.Forms.NumericUpDown minAgeUpDown;
        private System.Windows.Forms.CheckBox oldRoutesCheckBox;
        private System.Windows.Forms.Panel edscPanel;
        private System.Windows.Forms.Label edscLYLabel5;
        private System.Windows.Forms.Label edscLYLabel4;
        private System.Windows.Forms.Label edscLYLabel3;
        private System.Windows.Forms.Label edscLYLabel2;
        private System.Windows.Forms.TextBox refSysTextBox5;
        private System.Windows.Forms.Label refSysLabel5;
        private System.Windows.Forms.TextBox refSysTextBox4;
        private System.Windows.Forms.Label refSysLabel4;
        private System.Windows.Forms.TextBox refSysTextBox3;
        private System.Windows.Forms.Label refSysLabel3;
        private System.Windows.Forms.TextBox refSysTextBox2;
        private System.Windows.Forms.Label refSysLabel2;
        private System.Windows.Forms.Label edscLYLabel1;
        private System.Windows.Forms.TextBox refSysTextBox1;
        private System.Windows.Forms.Label refSysLabel1;
        private System.Windows.Forms.TextBox cmdrNameTextBox;
        private System.Windows.Forms.Label cmdrNameLabel;
        private System.Windows.Forms.Label loopIntLabel;
        private System.Windows.Forms.NumericUpDown loopIntBox;
        private System.Windows.Forms.NumericUpDown edscLYBox1;
        private System.Windows.Forms.NumericUpDown edscLYBox5;
        private System.Windows.Forms.NumericUpDown edscLYBox4;
        private System.Windows.Forms.NumericUpDown edscLYBox3;
        private System.Windows.Forms.NumericUpDown edscLYBox2;
        private System.Windows.Forms.CheckBox shortenCheckBox;
        private System.Windows.Forms.ComboBox shipsSoldBox;
        private System.Windows.Forms.NumericUpDown crFilterUpDown;
        private System.Windows.Forms.Label crFilterLabel;
        private System.Windows.Forms.ToolStripMenuItem pushEDSCToCSV;
        private System.Windows.Forms.NumericUpDown demandBox;
        private System.Windows.Forms.Label demandLabel;
        private System.Windows.Forms.CheckBox showJumpsCheckBox;
        private System.Windows.Forms.Button miscSettingsButton;
        private System.Windows.Forms.TabPage logPage;
        private System.Windows.Forms.DataGridView pilotsLogDataGrid;
        private System.Windows.Forms.LinkLabel trackerLinkLabel;
        private System.Windows.Forms.LinkLabel faqLinkLabel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.ToolStripMenuItem insertAtGridRow;
        private System.Windows.Forms.ToolStripMenuItem forceRefreshGridView;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem copySystemToSrc;
        private System.Windows.Forms.ToolStripMenuItem copySystemToDest;
        private System.Windows.Forms.ToolStripMenuItem removeAtGridRow;
        private System.Windows.Forms.ToolStripMenuItem forceResortMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copySystemToEDSC;
    }
}

